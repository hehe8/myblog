package com.blog.Service;

import com.blog.Dao.Impl.UserDaoImpl;
import com.blog.Dao.UserDao;
import com.blog.Entry.User;
import com.blog.Tools.Sha256;

import java.util.List;

public class UserService {
    UserDao userDao = new UserDaoImpl();

    public boolean add(String username, String password, String email) {
        Sha256 sha256 = new Sha256();
        password = sha256.getSHA256(password);
        User user = new User(username, password, email);
        return userDao.add(user);
    }

    public List<User> getUserFenYe(int pageNo, int pageSize){
        return userDao.getUserFenYe(pageNo,pageSize);
    }

    public boolean delete(String userId) {
        return userDao.delete(userId);
    }

    public boolean update(User user) {
        return userDao.update(user);
    }

    public List<User> getAllUser() {
        return userDao.getAllUser();
    }

    public User getUserById(String userId) {
        return userDao.getUserById(userId);
    }
    public boolean updPassword(User user) {
        return userDao.updPassword(user);
    }
    public User getUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    public int getRowCount() {
        return userDao.getRowCount();
    }

//    public static void main(String[] args) {
//        new UserService().add("test1","123123","test@qq.com");
//    }
}
