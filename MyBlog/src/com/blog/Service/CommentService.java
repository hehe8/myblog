package com.blog.Service;

import com.blog.Dao.CommentDao;
import com.blog.Dao.Impl.CommentDaoImpl;
import com.blog.Entry.Comment;

import java.util.List;

public class CommentService {
    CommentDao commentDao = new CommentDaoImpl();

    public boolean add(Comment comment) {
        return commentDao.add(comment);
    }


    public boolean delete(String commentId) {
        return commentDao.delete(commentId);
    }


    public boolean like(String commentId) {
        return commentDao.like(commentId);
    }


    public boolean update(Comment comment) {
        return commentDao.update(comment);
    }

    public List<Comment> getCommentsByBlogId(int blogId) {
        return commentDao.getCommentsByBlogId(blogId);
    }

    public List<Comment> getCommentsByTime(int num) {
        return commentDao.getCommentsByTime(num);
    }

    public List<Comment> getCommentsByCountBlogId(int num) {
        return commentDao.getCommentsByCountBlogId(num);
    }

    public List<Comment> getCommentsFenYe(int pageNo, int pageSize) {
        return commentDao.getCommentsFenYe(pageNo, pageSize);
    }

    public int getRowCount() {
        return commentDao.getRowCount();
    }
}
