package com.blog.Service;

import com.blog.Dao.Impl.MinganciDaoImpl;
import com.blog.Dao.MinganciDao;
import com.blog.Entry.Minganci;

import java.util.List;

public class MinganciService {
    MinganciDao minganciDao = new MinganciDaoImpl();

    public List<String> getAll() {
        return minganciDao.getAll();
    }
}
