package com.blog.Service;

import com.blog.Dao.CategoryDao;
import com.blog.Dao.Impl.CategoryDaoImpl;
import com.blog.Entry.Category;

import java.util.List;

public class CategoryService {

    CategoryDao categoryDao = new CategoryDaoImpl();

    public boolean add(Category category) {
        return categoryDao.add(category);
    }

    public boolean delete(String categoryId) {
        return categoryDao.delete(categoryId);
    }

    public boolean update(Category category) {
        return categoryDao.update(category);
    }
    public List<Category> getAllCategory() {
        return categoryDao.getAllCategory();
    }

    public List<Category> getCategoryFenYe(int pageNo, int pageSize) {
        return categoryDao.getCategoryFenYe(pageNo, pageSize);
    }

    public Category getCategoryById(int categoryId) {
        return categoryDao.getCategoryById(categoryId);
    }
    public int getRowCount() {
        return categoryDao.getRowCount();
    }

    }
