package com.blog.Service;

import com.blog.Dao.BlogDao;
import com.blog.Dao.Impl.BlogDaoImpl;
import com.blog.Entry.Blog;

import java.util.List;

public class BlogService {
    BlogDao blogDao = new BlogDaoImpl();

    public boolean add(Blog blog) {
        return blogDao.add(blog);
    }

    public boolean delete(String blogId) {
        return blogDao.delete(blogId);
    }


    public boolean update(Blog blog) {
        return blogDao.update(blog);
    }


    public List<Blog> getAllBlog() {
        return blogDao.getAllBlog();
    }


    public Blog getBlogById(String blogId) {
        blogDao.addReadCount(blogId);
        return blogDao.getBlogById(blogId);
    }

    public List<Blog> getBlogByCategoryId(String categoryId, int pageNo, int pageSize) {
        return blogDao.getBlogByCategoryId(categoryId, pageNo, pageSize);
    }

    public List<Blog> getBlogByBlogTitle(String blogTitle, int pageNo, int pageSize) {
        return blogDao.getBlogByBlogTitle(blogTitle, pageNo, pageSize);
    }


    public List<Blog> getBlogByBlogDate(String blogDateTime, int pageNo, int pageSize) {
        return blogDao.getBlogByBlogDate(blogDateTime, pageNo, pageSize);
    }


    public List<Blog> getBlogByBlogDate(int pageNo, int pageSize) {
        return blogDao.getBlogByBlogDate(pageNo, pageSize);
    }


    public List<Blog> getBlogByBlogReadCount(int pageSize) {
        return blogDao.getBlogByBlogReadCount(pageSize);
    }


    public List<Blog> getBlogFenYe(int pageNo, int pageSize) {
        return blogDao.getBlogFenYe(pageNo, pageSize);
    }


    public int getRowCount() {
        return blogDao.getRowCount();
    }


    public int getRowCountByBlogTitle(String blogTitle) {
        return blogDao.getRowCountByBlogTitle(blogTitle);
    }

    public int getRowCountByCategoryId(String categoryId) {
        return blogDao.getRowCountByCategoryId(categoryId);
    }

//    public static void main(String[] args) {
//        System.out.println(new BlogService().getBlogByBlogDate("2020-06",1,10));
//    }
}
