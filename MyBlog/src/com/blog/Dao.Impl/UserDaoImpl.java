package com.blog.Dao.Impl;

import com.blog.Dao.UserDao;
import com.blog.Entry.User;
import com.blog.Tools.DataBase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl extends DataBase implements UserDao {

    @Override
    public boolean add(User user) {
        String sql = "insert into user(username,password,email) values(?,?,?);";
        String username = user.getUsername();
        String password = user.getPassword();
        String email = user.getEmail();
        return this.executeUpdate(sql, username, password, email);
    }

    @Override
    public boolean delete(String userId) {
        String sql = "delete from user where userId=?";
        return this.executeUpdate(sql, userId);
    }

    @Override
    public boolean update(User user) {
        String sql = "update user set username=? ,email=?,admin=? where userId=?";
        int userId = user.getUserId();
        String username = user.getUsername();
        String email = user.getEmail();
        String admin = user.getAdmin();
        return this.executeUpdate(sql, username, email, admin, userId);
    }

    @Override
    public boolean updPassword(User user) {
        String sql = "update user set password=? where userId=?";
        int userId = user.getUserId();
        String password = user.getPassword();
        return this.executeUpdate(sql, password, userId);
    }

    @Override
    public List<User> getUserFenYe(int pageNo, int pageSize) {
        String sql = "select userId,username,password,email,admin from user ORDER BY userId  limit ?,? ";
        pageNo = (pageNo - 1) * pageSize;
        this.rs = this.executeQuery(sql, pageNo, pageSize);
        List<User> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                User user = new User();
                user.setUserId(rs.getInt("userId"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setEmail(rs.getString("email"));
                user.setAdmin(rs.getString("admin"));
                list.add(user);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<User> getAllUser() {
        String sql = "select * from user";
        List<User> list = new ArrayList<>();
        this.rs = this.executeQuery(sql);
        try {
            while (rs.next()) {
                User user = new User();
                user.setUserId(rs.getInt("userId"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setEmail(rs.getString("email"));
                user.setAdmin(rs.getString("admin"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public User getUserById(String userId) {
        String sql = "select * from user where userId=?";
        this.executeQuery(sql, userId);
        User user = null;
        try {
            while (rs.next()) {
                user = new User();
                user.setUserId(rs.getInt("userId"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setEmail(rs.getString("email"));
                user.setAdmin(rs.getString("admin"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }


    @Override
    public User getUserByUsername(String username) {
        String sql = "select * from user where username=?";
        this.executeQuery(sql, username);
        User user = null;
        try {
            while (rs.next()) {
                user = new User();
                user.setUserId(rs.getInt("userId"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setEmail(rs.getString("email"));
                user.setAdmin(rs.getString("admin"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        String sql = "select * from user where email=?";
        this.executeQuery(sql, email);
        User user = null;
        try {
            while (rs.next()) {
                user = new User();
                user.setUserId(rs.getInt("userId"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setEmail(rs.getString("email"));
                user.setAdmin(rs.getString("admin"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public int getRowCount() {
        String sql = "select count(userId) as rowCount from user";
        this.rs = this.executeQuery(sql);
        int count = 0;
        try {
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
}
