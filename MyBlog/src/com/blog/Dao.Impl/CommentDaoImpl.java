package com.blog.Dao.Impl;

import com.blog.Dao.CommentDao;
import com.blog.Entry.Category;
import com.blog.Entry.Comment;
import com.blog.Tools.DataBase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDaoImpl extends DataBase implements CommentDao {
    @Override
    public boolean add(Comment comment) {
        String sql = "insert into comment(blogId,commentcontent,commentname) values(?,?,?);";
        int blogId = comment.getBlogId();
        String commentContent = comment.getCommentContent();
        String commentName = comment.getCommentName();
        return this.executeUpdate(sql, blogId, commentContent, commentName);
    }

    @Override
    public boolean delete(String commentId) {
        String sql = "delete from comment where commentId=?";
        return this.executeUpdate(sql, commentId);
    }

    @Override
    public boolean like(String commentId) {
        String sql = "UPDATE COMMENT SET commentLike=commentLike+1 WHERE commentId=?";
        return this.executeUpdate(sql, commentId);
    }

    @Override
    public boolean update(Comment comment) {
        String sql = "update COMMENT set commentContent=? where commentid=?";
        int commentId = comment.getBlogId();
        String commentContent = comment.getCommentContent();
        return this.executeUpdate(sql, commentContent, commentId);
    }

    @Override
    public List<Comment> getCommentsByBlogId(int blogId) {
        String sql = "select commentId,blogId,commentContent,commentName,commentTime,commentLike from comment where blogId = ?  ORDER BY commenttime DESC ";
        this.rs = this.executeQuery(sql, blogId);
        List<Comment> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Comment comment = new Comment();
                comment.setCommentId(rs.getInt("commentId"));
                comment.setBlogId(rs.getInt("blogId"));
                comment.setCommentContent(rs.getString("commentContent"));
                comment.setCommentName(rs.getString("commentName"));
                comment.setCommentTime(rs.getString("commentTime"));
                comment.setCommentLike(rs.getInt("commentLike"));
                list.add(comment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<Comment> getCommentsByTime(int num) {
        String sql = "select commentId,blog.blogId,blogTitle,commentContent,commentName,commentTime from comment,blog where blog.blogId=comment.blogId  ORDER BY commentTime DESC limit 0,?";
        this.rs = this.executeQuery(sql, num);
        List<Comment> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Comment comment = new Comment();
                comment.setCommentId(rs.getInt("commentId"));
                comment.setBlogId(rs.getInt("blogId"));
                comment.setBlogTitle(rs.getString("blogTitle"));
                comment.setCommentContent(rs.getString("commentContent"));
                comment.setCommentName(rs.getString("commentName"));
                comment.setCommentTime(rs.getString("commentTime"));
                list.add(comment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<Comment> getCommentsFenYe(int pageNo, int pageSize) {
        String sql = "select commentId,blog.blogId,blogTitle,commentContent,commentName,commentTime,commentLike from comment,blog where blog.blogId=comment.blogId  ORDER BY commentTime DESC limit ?,?";
        pageNo =(pageNo-1)*pageSize;
        this.rs = this.executeQuery(sql, pageNo,pageSize);
        List<Comment> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Comment comment = new Comment();
                comment.setCommentId(rs.getInt("commentId"));
                comment.setBlogId(rs.getInt("blogId"));
                comment.setBlogTitle(rs.getString("blogTitle"));
                comment.setCommentContent(rs.getString("commentContent"));
                comment.setCommentName(rs.getString("commentName"));
                comment.setCommentTime(rs.getString("commentTime"));
                comment.setCommentLike(rs.getInt("commentLike"));
                list.add(comment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    @Override
    public List<Comment> getCommentsByCountBlogId(int num) {
        String sql = "select  blog.blogId ,blogTitle,count(1) as num from blog,comment where  comment.blogId = blog.blogId group by blogId order by num desc limit 0,?";
        this.rs = this.executeQuery(sql, num);
        List<Comment> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Comment comment = new Comment();
                comment.setBlogId(rs.getInt("blogId"));
                comment.setBlogTitle(rs.getString("blogTitle"));
                comment.setCountBlogId(rs.getInt("num"));
                list.add(comment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    @Override
    public int getRowCount() {
        String sql = "select count(commentId) as rowCount from comment";
        this.rs = this.executeQuery(sql);
        int count = 0;
        try {
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }


    public static void main(String[] args) {
//        List<Comment> list = new CommentDaoImpl().getCommentsByBlogId(5);
//        for (Comment c : list) {
//            System.out.println(c.getCommentId()+" "+c.getCommentLike());
//        }
        Comment comment=new Comment();
        comment.setBlogId(7);
        comment.setCommentContent("哈哈哈");
        comment.setCommentName("KHNLL");
        System.out.println(new CommentDaoImpl().add(comment));
    }
}
