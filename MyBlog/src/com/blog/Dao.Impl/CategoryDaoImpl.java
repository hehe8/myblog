package com.blog.Dao.Impl;

import com.blog.Dao.CategoryDao;
import com.blog.Entry.Blog;
import com.blog.Entry.Category;
import com.blog.Tools.DataBase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoryDaoImpl extends DataBase implements CategoryDao {
    @Override
    public boolean add(Category category) {
        String sql = "insert into category(categoryName,categoryContent) values(?,?);";
        String categoryName = category.getCategoryName();
        String categoryContent = category.getCategoryContent();
        return this.executeUpdate(sql, categoryName, categoryContent);
    }

    @Override
    public boolean delete(String categoryId) {
        String sql = "delete from category where categoryId=?";
        return this.executeUpdate(sql, categoryId);
    }

    @Override
    public boolean update(Category category) {
        String sql = "update category set categoryName=?,categoryContent=? where categoryId=?";
        int categoryId = category.getCategoryId();
        String categoryName = category.getCategoryName();
        String categoryContent = category.getCategoryContent();
        return this.executeUpdate(sql, categoryName,categoryContent, categoryId);
    }

    @Override
    public List<Category> getAllCategory() {
        String sql = "select * from category";
        List<Category> list = new ArrayList<>();
        this.rs = this.executeQuery(sql);
        try {
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt("categoryId"));
                category.setCategoryName(rs.getString("categoryName"));
                category.setCategoryContent(rs.getString("categoryContent"));
                list.add(category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<Category> getCategoryFenYe(int pageNo, int pageSize) {
        String sql = "select categoryId,categoryName,categoryContent,blogNum from category ORDER BY categoryId DESC limit ?,? ";
        pageNo = (pageNo - 1) * pageSize;
        this.rs = this.executeQuery(sql, pageNo, pageSize);
        List<Category> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Category category = new Category();
                category.setCategoryId(rs.getInt("categoryId"));
                category.setCategoryName(rs.getString("categoryName"));
                category.setCategoryContent(rs.getString("categoryContent"));
                category.setBlogNum(rs.getInt("blogNum"));
                list.add(category);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Category getCategoryById(int categoryId) {
        String sql = "select * from category where categoryId=?";
        this.executeQuery(sql,categoryId);
        Category category = null;
        try {
            while (rs.next()) {
                category = new Category();
                category.setCategoryId(rs.getInt("categoryId"));
                category.setCategoryName(rs.getString("categoryName"));
                category.setCategoryContent(rs.getString("categoryContent"));
                category.setBlogNum(rs.getInt("blogNum"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return category;
    }

    @Override
    public int getRowCount() {
        String sql = "select count(categoryId) as rowCount from category";
        this.rs = this.executeQuery(sql);
        int count = 0;
        try {
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
}
