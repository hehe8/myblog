package com.blog.Dao.Impl;

import com.blog.Dao.MinganciDao;
import com.blog.Entry.Minganci;
import com.blog.Tools.DataBase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MinganciDaoImpl extends DataBase implements MinganciDao {

    @Override
    public List<String> getAll() {
        String sql = "select * from minganci";
        List<String> list = new ArrayList<>();
        this.rs = this.executeQuery(sql);
        try {
            while (rs.next()) {
//                Minganci minganci = new Minganci();
//                minganci.setMgcId(rs.getInt("mgcId"));
//                minganci.setMgc(rs.getString("mgc"));
//                list.add(minganci);
                list.add(rs.getString("mgc"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
