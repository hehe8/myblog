package com.blog.Dao.Impl;

import com.blog.Dao.BlogDao;
import com.blog.Entry.Blog;
import com.blog.Tools.DataBase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BlogDaoImpl extends DataBase implements BlogDao {

    @Override
    public boolean add(Blog blog) {
        String sql = "    insert into blog(blogtitle,blogImgUrl,blogdatetime,blogsummary,blogcontent,categoryid) values(?,?,?,?,?,?);";
//        int blogId=blog.getBlogId();
        String blogTitle = blog.getBlogTitle();
        String blogImgUrl = blog.getBlogImgUrl();
        String blogDateTime = blog.getBlogDateTime();
        String blogSummary = blog.getBlogSummary();
        String blogContent = blog.getBlogContent();
        int categoryId = blog.getCategoryId();
        return this.executeUpdate(sql, blogTitle, blogImgUrl, blogDateTime, blogSummary, blogContent, categoryId);
    }

    @Override
    public boolean delete(String blogId) {
        String sql = "delete from blog where blogId=?";
        return this.executeUpdate(sql, blogId);
    }

    @Override
    public boolean update(Blog blog) {
        String sql = "update blog set blogTitle=?,blogImgUrl=?,blogDateTime=?,blogSummary=?,blogContent=?,categoryId=? where blogId=?";
        int blogId = blog.getBlogId();
        String blogTitle = blog.getBlogTitle();
        String blogImgUrl = blog.getBlogImgUrl();
        String blogDateTime = blog.getBlogDateTime();
        String blogSummary = blog.getBlogSummary();
        String blogContent = blog.getBlogContent();
        int categoryId = blog.getCategoryId();
        return this.executeUpdate(sql, blogTitle, blogImgUrl, blogDateTime, blogSummary, blogContent, categoryId, blogId);
    }

    @Override
    public boolean addReadCount(String blogId) {
        String sql="update blog set blogReadCount=blogReadCount+1 where blogId=?";
        return this.executeUpdate(sql, blogId);
    }

    @Override
    public List<Blog> getAllBlog() {
        String sql = "select * from blog";
        List<Blog> list = new ArrayList<>();
        this.rs = this.executeQuery(sql);
        try {
            while (rs.next()) {
                Blog blog = new Blog();
                blog.setBlogId(rs.getInt("blogId"));
                blog.setBlogTitle(rs.getString("blogTitle"));
                blog.setBlogImgUrl(rs.getString("blogImgUrl"));
                blog.setBlogDateTime(rs.getString("blogDateTime"));
                blog.setBlogSummary(rs.getString("blogSummary"));
                blog.setBlogContent(rs.getString("blogContent"));
                blog.setBlogReadCount(rs.getInt("blogReadCount"));
                blog.setCategoryId(rs.getInt("categoryId"));
                list.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Blog getBlogById(String blogId) {
        String sql = "select * from blog where blogId=?";
        this.executeQuery(sql, blogId);
        Blog blog = null;
        try {
            while (rs.next()) {
                blog = new Blog();
                blog.setBlogId(rs.getInt("blogId"));
                blog.setBlogTitle(rs.getString("blogTitle"));
                blog.setBlogImgUrl(rs.getString("blogImgUrl"));
                blog.setBlogDateTime(rs.getString("blogDateTime"));
                blog.setBlogSummary(rs.getString("blogSummary"));
                blog.setBlogContent(rs.getString("blogContent"));
                blog.setBlogReadCount(rs.getInt("blogReadCount"));
                blog.setCommentNum(rs.getInt("CommentNum"));
                blog.setCategoryId(rs.getInt("categoryId"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return blog;
    }

    @Override
    public List<Blog> getBlogByCategoryId(String categoryId,int pageNo, int pageSize) {
        String sql = "select * from blog where categoryId=?  ORDER BY blogDateTime DESC limit ?,?";
        pageNo = (pageNo - 1) * pageSize;
        this.rs = this.executeQuery(sql,categoryId,pageNo,pageSize);
        List<Blog> list=new ArrayList<>();
        try {
            Blog blog = null;
            while (rs.next()) {
                blog = new Blog();
                blog.setBlogId(rs.getInt("blogId"));
                blog.setBlogTitle(rs.getString("blogTitle"));
                blog.setBlogImgUrl(rs.getString("blogImgUrl"));
                blog.setBlogDateTime(rs.getString("blogDateTime"));
                blog.setBlogSummary(rs.getString("blogSummary"));
                blog.setBlogContent(rs.getString("blogContent"));
                blog.setBlogReadCount(rs.getInt("blogReadCount"));
                blog.setCommentNum(rs.getInt("CommentNum"));
                blog.setCategoryId(rs.getInt("categoryId"));
                list.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<Blog> getBlogByBlogTitle(String blogTitle,int pageNo,int pageSize) {
        String sql = "select blogId,blogTitle,blogImgUrl,blogDateTime,blogSummary,blogReadCount,CommentNum, categoryId from blog where blogTitle like '%" + blogTitle + "%' ORDER BY blogDateTime DESC limit ?,?";
        pageNo = (pageNo - 1) * pageSize;
        this.rs = this.executeQuery(sql,pageNo,pageSize);
        List<Blog> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Blog blog = new Blog();
                blog.setBlogId(rs.getInt("blogId"));
                blog.setBlogTitle(rs.getString("blogTitle"));
                blog.setBlogImgUrl(rs.getString("blogImgUrl"));
                blog.setBlogDateTime(rs.getString("blogDateTime"));
                blog.setBlogSummary(rs.getString("blogSummary"));
                blog.setBlogReadCount(rs.getInt("blogReadCount"));
                blog.setCommentNum(rs.getInt("CommentNum"));
                blog.setCategoryId(rs.getInt("categoryId"));
                list.add(blog);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;

    }

    @Override
    public List<Blog> getBlogByBlogDate(String blogDateTime, int pageNo, int pageSize) {
        String sql = "SELECT blogId,blogTitle,blogImgUrl,blogDateTime,blogSummary,blogReadCount,CommentNum,categoryId FROM blog WHERE    SUBSTRING(blogdatetime, 1, 7)=? ORDER BY blogdatetime DESC LIMIT ?,? ";
        pageNo = (pageNo - 1) * pageSize;
        this.rs = this.executeQuery(sql,blogDateTime, pageNo, pageSize);
        List<Blog> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Blog blog = new Blog();
                blog.setBlogId(rs.getInt("blogId"));
                blog.setBlogTitle(rs.getString("blogTitle"));
                blog.setBlogImgUrl(rs.getString("blogImgUrl"));
                blog.setBlogDateTime(rs.getString("blogDateTime"));
                blog.setBlogSummary(rs.getString("blogSummary"));
                blog.setBlogReadCount(rs.getInt("blogReadCount"));
                blog.setCommentNum(rs.getInt("CommentNum"));
                blog.setCategoryId(rs.getInt("categoryId"));
                list.add(blog);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<Blog> getBlogByBlogDate(int pageNo,int pageSize) {
        String sql = "SELECT substring(blogDatetime, 1, 7)as date,COUNT(0) as num FROM blog GROUP BY  substring(blogDatetime, 1, 7) order by date desc limit ?,?;";
        pageNo = (pageNo - 1) * pageSize;
        this.rs = this.executeQuery(sql, pageNo, pageSize);
        List<Blog> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Blog blog = new Blog();
                blog.setBlogDateTime(rs.getString("date"));
                blog.setCommentNum(rs.getInt("num"));
                list.add(blog);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<Blog> getBlogByBlogReadCount( int pageSize ) {
        String sql = "select blogId,blogTitle,blogReadCount from blog ORDER BY blogReadCount DESC limit 0,? ";
        this.rs = this.executeQuery(sql,pageSize);
        List<Blog> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Blog blog = new Blog();
                blog.setBlogId(rs.getInt("blogId"));
                blog.setBlogTitle(rs.getString("blogTitle"));
                blog.setBlogReadCount(rs.getInt("blogReadCount"));
                list.add(blog);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public List<Blog> getBlogFenYe(int pageNo, int pageSize) {
        String sql = "select blogId,blogTitle,blogImgUrl,blogDateTime,blogSummary,blogReadCount,CommentNum from blog ORDER BY blogDateTime DESC limit ?,? ";
        pageNo = (pageNo - 1) * pageSize;
        this.rs = this.executeQuery(sql, pageNo, pageSize);
        List<Blog> list = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                Blog blog = new Blog();
                blog.setBlogId(rs.getInt("blogId"));
                blog.setBlogTitle(rs.getString("blogTitle"));
                blog.setBlogImgUrl(rs.getString("blogImgUrl"));
                blog.setBlogDateTime(rs.getString("blogDateTime"));
                blog.setBlogSummary(rs.getString("blogSummary"));
                blog.setBlogReadCount(rs.getInt("blogReadCount"));
                blog.setCommentNum(rs.getInt("CommentNum"));
                list.add(blog);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public int getRowCount() {
        String sql = "select count(blogId) as rowCount from blog";
        this.rs = this.executeQuery(sql);
        int count = 0;
        try {
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public int getRowCountByBlogTitle(String blogTitle) {
        String sql = "select count(blogId) from blog where blogTitle like '%" + blogTitle + "%' ";
        this.rs = this.executeQuery(sql);
        int count = 0;
        try {
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public int getRowCountByCategoryId(String categoryId) {
        String sql = "select count(blogId) from blog where categoryId=?";
        this.rs = this.executeQuery(sql,categoryId);
        int count = 0;
        try {
            if (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

//    public static void main(String[] args) {
//        BlogDaoImpl blogDao = new BlogDaoImpl();
//        System.out.println(blogDao.getAllBlog());
//        System.out.println(blogDao.getBlogById("19"));
//        System.out.println(blogDao.update(new Blog(1,"1","11-11-11 12:12:12","1","1",1)));
//          List<Blog> list= blogDao.getBlogByCategoryId("1",1,10);
//          for (Blog blog:list){
//              System.out.println(blog.getBlogTitle());
//              System.out.println(blog.getBlogDateTime());
//          }
//        Blog blog=new Blog();
//        blog.setCategoryId(1);
//        blog.setBlogContent("2");
//        blog.setBlogSummary("3");
//        blog.setBlogDateTime("2020-06-12 03:02:13");
//        blog.setBlogTitle("4");
//        blog.setBlogImgUrl("img/1102665-20181104182953302-371660734.jpg");
//        blog.setBlogId(3);
//        System.out.println(blogDao.update(blog));
//    }
}

