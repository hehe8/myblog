package com.blog.Entry;

public class Minganci {
    private int mgcId;
    private String mgc;

    public Minganci() {
    }

    public Minganci(int mgcId, String mgc) {
        this.mgcId = mgcId;
        this.mgc = mgc;
    }

    public int getMgcId() {
        return mgcId;
    }

    public void setMgcId(int mgcId) {
        this.mgcId = mgcId;
    }

    public String getMgc() {
        return mgc;
    }

    public void setMgc(String mgc) {
        this.mgc = mgc;
    }
}
