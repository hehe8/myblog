package com.blog.Entry;

public class Blog {
    private int blogId;
    private String blogTitle;
    private String blogImgUrl;
    private String blogDateTime;
    private String blogSummary;
    private String blogContent;
    private int blogReadCount;
    private int commentNum;
    private int categoryId;

    public Blog() {
    }

    public Blog(String blogTitle, String blogImgUrl, String blogDateTime, String blogSummary, String blogContent, int blogReadCount) {
        this.blogTitle = blogTitle;
        this.blogImgUrl = blogImgUrl;
        this.blogDateTime = blogDateTime;
        this.blogSummary = blogSummary;
        this.blogContent = blogContent;
        this.blogReadCount = blogReadCount;
    }

    public Blog(int blogId, String blogTitle, String blogImgUrl, String blogDateTime, String blogSummary, String blogContent, int blogReadCount) {
        this.blogId = blogId;
        this.blogTitle = blogTitle;
        this.blogImgUrl = blogImgUrl;
        this.blogDateTime = blogDateTime;
        this.blogSummary = blogSummary;
        this.blogContent = blogContent;
        this.blogReadCount = blogReadCount;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(int commentNum) {
        this.commentNum = commentNum;
    }

    public String getBlogImgUrl() {
        return blogImgUrl;
    }

    public void setBlogImgUrl(String blogImgUrl) {
        this.blogImgUrl = blogImgUrl;
    }

    public String getBlogDateTime() {
        return blogDateTime;
    }

    public void setBlogDateTime(String blogDateTime) {
        this.blogDateTime = blogDateTime;
    }

    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }


    public String getBlogSummary() {
        return blogSummary;
    }

    public void setBlogSummary(String blogSummary) {
        this.blogSummary = blogSummary;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    public int getBlogReadCount() {
        return blogReadCount;
    }

    public void setBlogReadCount(int blogReadCount) {
        this.blogReadCount = blogReadCount;
    }
}
