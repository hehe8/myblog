package com.blog.Entry;

public class Comment {
    private int commentId;
    private int blogId;
    private String blogTitle;
    private String commentContent;
    private String commentName;
    private String commentTime;
    private String commentEmail;
    private String commentUrl;
    private int countBlogId;
    private int commentLike;

    public Comment() {
    }
    public Comment(int commentId, int blogId, String commentContent, String commentName, String commentTime, String commentEmail, String commentUrl) {
        this.commentId = commentId;
        this.blogId = blogId;
        this.commentContent = commentContent;
        this.commentName = commentName;
        this.commentTime = commentTime;
        this.commentEmail = commentEmail;
        this.commentUrl = commentUrl;
    }

    public Comment(int commentId, int blogId, String blogTitle, String commentContent, String commentName, String commentTime, String commentEmail, String commentUrl, int countBlogId, int commentLike) {
        this.commentId = commentId;
        this.blogId = blogId;
        this.blogTitle = blogTitle;
        this.commentContent = commentContent;
        this.commentName = commentName;
        this.commentTime = commentTime;
        this.commentEmail = commentEmail;
        this.commentUrl = commentUrl;
        this.countBlogId = countBlogId;
        this.commentLike = commentLike;
    }

    public int getCommentLike() {
        return commentLike;
    }

    public void setCommentLike(int commentLike) {
        this.commentLike = commentLike;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public int getCountBlogId() {
        return countBlogId;
    }

    public void setCountBlogId(int countBlogId) {
        this.countBlogId = countBlogId;
    }

    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getCommentName() {
        return commentName;
    }

    public void setCommentName(String commentName) {
        this.commentName = commentName;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public String getCommentEmail() {
        return commentEmail;
    }

    public void setCommentEmail(String commentEmail) {
        this.commentEmail = commentEmail;
    }

    public String getCommentUrl() {
        return commentUrl;
    }

    public void setCommentUrl(String commentUrl) {
        this.commentUrl = commentUrl;
    }
}
