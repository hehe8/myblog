package com.blog.Entry;

public class Category {
    private int categoryId;
    private String categoryName;
    private String categoryContent;
    private int blogNum;

    public Category() {
    }

    public Category(int categoryId, String categoryName, String categoryContent, int blogNum) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryContent = categoryContent;
        this.blogNum = blogNum;
    }

    public Category(int categoryId, String categoryName, String categoryContent) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryContent = categoryContent;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getBlogNum() {
        return blogNum;
    }

    public void setBlogNum(int blogNum) {
        this.blogNum = blogNum;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryContent() {
        return categoryContent;
    }

    public void setCategoryContent(String categoryContent) {
        this.categoryContent = categoryContent;
    }
}
