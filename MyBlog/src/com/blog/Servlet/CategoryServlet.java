package com.blog.Servlet;


import com.blog.Entry.Blog;
import com.blog.Service.BlogService;
import com.blog.Service.CategoryService;
import com.blog.Entry.Category;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/CategoryServlet")
public class CategoryServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "add":
                add(req, resp);
                break;
            case "upd":
                update(req, resp);
                break;
            case "del":
                delete(req, resp);
                break;
            case "list":
                getAllCategory(req, resp);
                break;
            case "fenye":
                getCategoryFenYe(req, resp);
                break;
            case "id":
                getCategoryById(req, resp);
                break;
            default:
                break;
        }
    }

    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CategoryService categoryService = new CategoryService();
        String name = req.getParameter("name");
        String content = req.getParameter("content");
        Category category = new Category();
        category.setCategoryName(name);
        category.setCategoryContent(content);
        boolean b = categoryService.add(category);
        if (b) {
            out.println(jsonp + "({\"code\":\"200\",\"message\":\"添加成功\",\"errors\":\"0\"})");
        } else {
            out.println(jsonp + "({\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CategoryService categoryService = new CategoryService();
        String categoryId = req.getParameter("categoryId");
        boolean b = categoryService.delete(categoryId);
        if (b) {
            out.println(jsonp + "({\"code\":\"200\",\"message\":\"删除成功\",\"errors\":\"0\"})");
        } else {
            out.println(jsonp + "({\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CategoryService categoryService = new CategoryService();
        int categoryId = Integer.parseInt(req.getParameter("categoryid"));
        String name = req.getParameter("name");
        String content = req.getParameter("content");
        Category category = new Category();
        category.setCategoryId(categoryId);
        category.setCategoryName(name);
        category.setCategoryContent(content);
        boolean b = categoryService.update(category);
        if (b) {
            out.println(jsonp + "({\"code\":\"200\",\"message\":\"更新成功\",\"errors\":\"0\"})");
        } else {
            out.println(jsonp + "({\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void getAllCategory(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CategoryService categoryService = new CategoryService();
        List<Category> list = categoryService.getAllCategory();
        StringBuffer buffer = new StringBuffer();
        int i = 1;
        for (Category category : list) {
            buffer.append("{");
            buffer.append("\"categoryId\":\"" + category.getCategoryId() + "\",");
            buffer.append("\"categoryName\":\"" + category.getCategoryName() + "\",");
            buffer.append("\"categoryContent\":\"" + category.getCategoryContent() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([" + buffer + "])");
    }

    protected void getCategoryById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        String categoryId = req.getParameter("id");
        CategoryService categoryService=new CategoryService();
        Category category = categoryService.getCategoryById(Integer.parseInt(categoryId));
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        buffer.append("\"categoryId\":\"" + category.getCategoryId() + "\",");
        buffer.append("\"categoryName\":\"" + category.getCategoryName() + "\",");
        buffer.append("\"categoryContent\":\"" + category.getCategoryContent() + "\",");
        buffer.append("\"blogNum\":\"" + category.getBlogNum() + "\"");
        buffer.append("}");
        out.print(jsonp + "(" + buffer + ")");
    }

    protected void getCategoryByCategoryName(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    protected void getCategoryFenYe(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CategoryService categoryService = new CategoryService();
        int pageNo = Integer.parseInt(req.getParameter("pageNo"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        List<Category> list = categoryService.getCategoryFenYe(pageNo, pageSize);
        StringBuffer buffer = new StringBuffer();
        int count = categoryService.getRowCount();
        int i = 1;
        for (Category category : list) {
            buffer.append("{");
            buffer.append("\"categoryId\":\"" + category.getCategoryId() + "\",");
            buffer.append("\"categoryName\":\"" + category.getCategoryName() + "\",");
            buffer.append("\"categoryContent\":\"" + category.getCategoryContent() + "\",");
            buffer.append("\"blogNum\":\"" + category.getBlogNum() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([{\"page\":\"" + pageNo + "\",\"total\":\"" + count + "\"}," + buffer + "])");
    }
}

