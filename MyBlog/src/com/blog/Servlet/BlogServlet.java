package com.blog.Servlet;

import com.blog.Entry.Category;
import com.blog.Service.BlogService;
import com.blog.Entry.Blog;
import com.blog.Service.CategoryService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/BlogServlet")
public class BlogServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        System.out.println(action);
        switch (action) {
            case "add":
                add(req, resp);
                break;
            case "upd":
                update(req, resp);
                break;
            case "del":
                delete(req, resp);
                break;
            case "list":
                getBlogFenYe(req, resp);
                break;
            case "title":
                getBlogByBlogTitle(req, resp);
                break;
            case "read":
                getBlogByBlogReadCount(req, resp);
                break;
            case "date":
                getBlogByBlogDate(req, resp);
                break;
            case "datelist":
                getBlogByBlogDateList(req, resp);
                break;
            case "id":
                getBlogById(req, resp);
                break;
            case "categoryid":
                getBlogByCategoryId(req, resp);
                break;
            default:
                break;
        }
    }

    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
//        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        String title = req.getParameter("title");
        String time = req.getParameter("time");
        String summary = req.getParameter("summary");
        String imgUrl = req.getParameter("imgUrl");
        String content = req.getParameter("content");
        String categoryId = req.getParameter("categoryId");
        Blog blog = new Blog();
        blog.setBlogTitle(title);
        blog.setBlogDateTime(time);
        blog.setBlogSummary(summary);
        blog.setBlogContent(content);
        blog.setBlogImgUrl(imgUrl);
        blog.setCategoryId(Integer.parseInt(categoryId));
        boolean b = blogService.add(blog);
        if (b) {
//            out.println(jsonp + "({\"code\":\"200\",\"message\":\"添加成功\",\"errors\":\"0\"})");
            out.println("{\"code\":\"200\",\"message\":\"添加成功\",\"errors\":\"0\"}");
        } else {
//            out.println(jsonp + "({\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
            out.println("{\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"}");
        }
    }

    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        String blogId = req.getParameter("blogId");
        boolean b = blogService.delete(blogId);
        if (b) {
            out.println(jsonp + "({\"code\":\"200\",\"message\":\"删除成功\",\"errors\":\"0\"})");
        } else {
            out.println(jsonp + "({\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
//        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        int blogId = Integer.parseInt(req.getParameter("blogid"));
        String title = req.getParameter("title");
        String time = req.getParameter("time");
        String summary = req.getParameter("summary");
        String categoryId = req.getParameter("categoryId");
        String content = req.getParameter("content");
        String imgUrl = req.getParameter("imgUrl");
        Blog blog = new Blog();
        blog.setBlogId(blogId);
        blog.setBlogTitle(title);
        blog.setBlogDateTime(time);
        blog.setBlogSummary(summary);
        blog.setBlogContent(content);
        blog.setBlogImgUrl(imgUrl);
        blog.setCategoryId(Integer.parseInt(categoryId));
        boolean b = blogService.update(blog);
        if (b) {
//            out.println(jsonp + "({\"code\":\"200\",\"message\":\"更新成功\",\"errors\":\"0\"})");
            out.println("{\"code\":\"200\",\"message\":\"更新成功\",\"errors\":\"0\"}");
        } else {
//            out.println(jsonp + "({\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
            out.println("{\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"}");
        }
    }

    protected void getAllBlog(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    protected void getBlogById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        String blogId = req.getParameter("blogid");
        Blog blog = blogService.getBlogById(blogId);
        CategoryService categoryService=new CategoryService();
        Category category = categoryService.getCategoryById(blog.getCategoryId());
         StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        buffer.append("\"blogId\":\"" + blog.getBlogId() + "\",");
        buffer.append("\"blogTitle\":\"" + blog.getBlogTitle() + "\",");
        buffer.append("\"blogImgUrl\":\"" + blog.getBlogImgUrl() + "\",");
        buffer.append("\"blogDateTime\":\"" + blog.getBlogDateTime() + "\",");
        buffer.append("\"blogSummary\":\"" + blog.getBlogSummary() + "\",");
        buffer.append("\"blogContent\":\"" + blog.getBlogContent() + "\",");
        buffer.append("\"blogReadCount\":\"" + blog.getBlogReadCount() + "\",");
        buffer.append("\"commentNum\":\"" + blog.getCommentNum() + "\",");
        buffer.append("\"categoryId\":\"" + blog.getCategoryId() + "\",");
        buffer.append("\"categoryName\":\"" + category.getCategoryName() + "\",");
        buffer.append("\"categoryContent\":\"" + category.getCategoryContent() + "\",");
        buffer.append("\"blogNum\":\"" + category.getBlogNum() + "\"");
        buffer.append("}");
        out.print(jsonp + "(" + buffer + ")");
    }

    protected void getBlogByCategoryId(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        String categoryId = req.getParameter("categoryid");
        int pageNo = Integer.parseInt(req.getParameter("pageNo"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        List<Blog> list = blogService.getBlogByCategoryId(categoryId, pageNo, pageSize);
        StringBuffer buffer = new StringBuffer();
        int count = blogService.getRowCountByCategoryId(categoryId);
        int i = 1;
        for (Blog blog : list) {
            buffer.append("{");
            buffer.append("\"blogId\":\"" + blog.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + blog.getBlogTitle() + "\",");
            buffer.append("\"blogImgUrl\":\"" + blog.getBlogImgUrl() + "\",");
            buffer.append("\"blogDateTime\":\"" + blog.getBlogDateTime() + "\",");
            buffer.append("\"blogSummary\":\"" + blog.getBlogSummary() + "\",");
            buffer.append("\"blogReadCount\":\"" + blog.getBlogReadCount() + "\",");
            buffer.append("\"categoryId\":\"" + blog.getCategoryId() + "\",");
            buffer.append("\"commentNum\":\"" + blog.getCommentNum() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([{\"page\":\"" + pageNo + "\",\"total\":\"" + count + "\"}," + buffer + "])");
    }

    protected void getBlogByBlogTitle(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        int pageNo = Integer.parseInt(req.getParameter("pageNo"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        String title = req.getParameter("title");
        List<Blog> list = blogService.getBlogByBlogTitle(title, pageNo, pageSize);
        StringBuffer buffer = new StringBuffer();
        int count = blogService.getRowCountByBlogTitle(title);
        int i = 1;
        for (Blog blog : list) {
            buffer.append("{");
            buffer.append("\"blogId\":\"" + blog.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + blog.getBlogTitle() + "\",");
            buffer.append("\"blogImgUrl\":\"" + blog.getBlogImgUrl() + "\",");
            buffer.append("\"blogDateTime\":\"" + blog.getBlogDateTime() + "\",");
            buffer.append("\"blogSummary\":\"" + blog.getBlogSummary() + "\",");
            buffer.append("\"blogReadCount\":\"" + blog.getBlogReadCount() + "\",");
            buffer.append("\"categoryId\":\"" + blog.getCategoryId() + "\",");
            buffer.append("\"commentNum\":\"" + blog.getCommentNum() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([{\"page\":\"" + pageNo + "\",\"total\":\"" + count + "\"}," + buffer + "])");
    }

    protected void getBlogByBlogDate(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        int pageNo = Integer.parseInt(req.getParameter("pageNo"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        String date = req.getParameter("date");
        List<Blog> list = blogService.getBlogByBlogDate(date, pageNo, pageSize);
        StringBuffer buffer = new StringBuffer();
        List<Blog> list1 = blogService.getBlogByBlogDate(1, 1000);
        int count = 0;
        for (Blog blog : list1) {
            if (date.equals(blog.getBlogDateTime())) {
                count = blog.getCommentNum();
                break;
            }
        }
        int i = 1;
        for (Blog blog : list) {
            buffer.append("{");
            buffer.append("\"blogId\":\"" + blog.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + blog.getBlogTitle() + "\",");
            buffer.append("\"blogImgUrl\":\"" + blog.getBlogImgUrl() + "\",");
            buffer.append("\"blogDateTime\":\"" + blog.getBlogDateTime() + "\",");
            buffer.append("\"blogSummary\":\"" + blog.getBlogSummary() + "\",");
            buffer.append("\"blogReadCount\":\"" + blog.getBlogReadCount() + "\",");
            buffer.append("\"categoryId\":\"" + blog.getCategoryId() + "\",");
            buffer.append("\"commentNum\":\"" + blog.getCommentNum() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([{\"page\":\"" + pageNo + "\",\"total\":\"" + count + "\"}," + buffer + "])");
    }

    protected void getBlogByBlogDateList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        int pageNo = Integer.parseInt(req.getParameter("pageNo"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        List<Blog> list = blogService.getBlogByBlogDate(pageNo, pageSize);
        StringBuffer buffer = new StringBuffer();
        int i = 1;
        for (Blog blog : list) {
            buffer.append("{");
            buffer.append("\"date\":\"" + blog.getBlogDateTime() + "\",");
            buffer.append("\"num\":\"" + blog.getCommentNum() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([" + buffer + "])");
    }

    protected void getBlogByBlogReadCount(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        List<Blog> list = blogService.getBlogByBlogReadCount(pageSize);
        StringBuffer buffer = new StringBuffer();
        int count = blogService.getRowCount();
        int i = 1;
        for (Blog blog : list) {
            buffer.append("{");
            buffer.append("\"blogId\":\"" + blog.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + blog.getBlogTitle() + "\",");
            buffer.append("\"blogReadCount\":\"" + blog.getBlogReadCount() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([" + buffer + "])");
    }

    protected void getBlogFenYe(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        BlogService blogService = new BlogService();
        int pageNo = Integer.parseInt(req.getParameter("pageNo"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        List<Blog> list = blogService.getBlogFenYe(pageNo, pageSize);
        StringBuffer buffer = new StringBuffer();
        int count = blogService.getRowCount();
        int i = 1;
        for (Blog blog : list) {
            buffer.append("{");
            buffer.append("\"blogId\":\"" + blog.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + blog.getBlogTitle() + "\",");
            buffer.append("\"blogImgUrl\":\"" + blog.getBlogImgUrl() + "\",");
            buffer.append("\"blogDateTime\":\"" + blog.getBlogDateTime() + "\",");
            buffer.append("\"blogSummary\":\"" + blog.getBlogSummary() + "\",");
            buffer.append("\"blogReadCount\":\"" + blog.getBlogReadCount() + "\",");
            buffer.append("\"commentNum\":\"" + blog.getCommentNum() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([{\"page\":\"" + pageNo + "\",\"total\":\"" + count + "\"}," + buffer + "])");
    }

}
