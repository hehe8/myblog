package com.blog.Servlet;

import com.blog.Entry.User;
import com.blog.Service.UserService;
import com.blog.Tools.Sha256;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class UserInfoServlet
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "id":
                getUserById(request, response);
                break;
            case "login":
                login(request, response);
                break;
            case "upd":
                update(request, response);
                break;
            case "password":
                updPassword(request, response);
                break;
            case "del":
                delete(request, response);
                break;
            case "list":
                getUserFenYe(request, response);
                break;
            case "register":
                register(request, response);
                break;
            default:
                break;
        }
    }

    protected void getUserById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        String userId = req.getParameter("id");
        UserService userService = new UserService();
        User user = userService.getUserById(userId);
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        buffer.append("\"userId\":\"" + user.getUserId() + "\",");
        buffer.append("\"username\":\"" + user.getUsername() + "\",");
        buffer.append("\"email\":\"" + user.getEmail() + "\",");
        buffer.append("\"admin\":\"" + user.getAdmin() + "\"");
        buffer.append("}");
        out.print(jsonp + "(" + buffer + ")");
    }

    protected void getUserFenYe(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        UserService userService = new UserService();
        int pageNo = Integer.parseInt(req.getParameter("pageNo"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        List<User> list = userService.getUserFenYe(pageNo, pageSize);
        StringBuffer buffer = new StringBuffer();
        int count = userService.getRowCount();
        int i = 1;
        for (User user : list) {
            buffer.append("{");
            buffer.append("\"userId\":\"" + user.getUserId() + "\",");
            buffer.append("\"username\":\"" + user.getUsername() + "\",");
            buffer.append("\"admin\":\"" + user.getAdmin() + "\",");
            buffer.append("\"password\":\"" + user.getPassword() + "\",");
            buffer.append("\"email\":\"" + user.getEmail() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([{\"page\":\"" + pageNo + "\",\"total\":\"" + count + "\"}," + buffer + "])");
    }


    protected void updPassword(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        String userId = req.getParameter("userid");
        String oldpassword = req.getParameter("oldpassword");
        UserService userService = new UserService();
        User user = userService.getUserById(userId);
        Sha256 sha256 = new Sha256();
        oldpassword = sha256.getSHA256(oldpassword);
        if (user.getPassword().equals(oldpassword)) {
            String password = req.getParameter("password");
            User user1 = new User();
            user1.setUserId(Integer.parseInt(userId));
            user1.setPassword(sha256.getSHA256(password));
            boolean b = userService.updPassword(user1);
            if (b) {
                out.print(jsonp + "({\"code\":\"200\",\"message\":\"更新成功\",\"errors\":\"1\"})");
            }else
                out.print(jsonp + "({\"code\":\"703\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        } else {
            out.print(jsonp + "({\"code\":\"701\",\"message\":\"密码错误\",\"errors\":\"1\"})");
        }
    }

    protected void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        String userId = req.getParameter("userid");
        String username = req.getParameter("username");
        String admin = req.getParameter("admin");
        String email = req.getParameter("email");
        UserService userService = new UserService();
        User user0=userService.getUserById(userId);
        User user = userService.getUserByUsername(username);
        User user1 = userService.getUserByEmail(email);
        if (user!=null&&(!user.getUsername().equals(user0.getUsername()))) {
            out.print(jsonp + "({\"code\":\"701\",\"message\":\"用户名已存在\",\"errors\":\"1\"})");
        } else if (user1 != null&&(!user1.getEmail().equals(user0.getEmail()))) {
            out.print(jsonp + "({\"code\":\"702\",\"message\":\"邮箱已存在\",\"errors\":\"1\"})");
        } else {
            User user2 = new User();
            user2.setUserId(Integer.parseInt(userId));
            user2.setUsername(username);
            user2.setEmail(email);
            user2.setAdmin(admin);
            boolean b = userService.update(user2);
            if (b) {
                out.print(jsonp + "({\"code\":\"200\",\"message\":\"更新成功\",\"userid\":\"" + user2.getUserId() + "\",\"username\":\"" +
                        user2.getUsername() + "\",\"admin\":\"" + user2.getAdmin() + "\",\"errors\":\"0\"})");
            } else {
                out.print(jsonp + "({\"code\":\"703\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
            }
        }
    }

    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        UserService userService = new UserService();
        String userId = req.getParameter("userId");
        boolean b = userService.delete(userId);
        if (b) {
            out.println(jsonp + "({\"code\":\"200\",\"message\":\"删除成功\",\"errors\":\"0\"})");
        } else {
            out.println(jsonp + "({\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String jsonp = request.getParameter("jsonCallback");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Sha256 sha256 = new Sha256();
        password = sha256.getSHA256(password);
        UserService userService = new UserService();
        User user = null;
        user = userService.getUserByUsername(username);
        if (user == null) {
            out.print(jsonp + "({\"code\":\"703\",\"message\":\"用户名错误\",\"errors\":\"1\"})");
        } else if (!user.getPassword().equals(password)) {
            out.print(jsonp + "({\"code\":\"703\",\"message\":\"密码错误\",\"errors\":\"1\"})");
        } else {
            out.print(jsonp + "({\"code\":\"200\",\"message\":\"OK\",\"userid\":\"" + user.getUserId() + "\",\"username\":\"" +
                    user.getUsername() + "\",\"admin\":\"" + user.getAdmin() + "\",\"errors\":\"0\"})");
        }
    }

    protected void register(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String jsonp = request.getParameter("jsonCallback");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        UserService userService = new UserService();
        User user = userService.getUserByUsername(username);
        User user1 = userService.getUserByEmail(email);
        if (user != null) {
            out.print(jsonp + "({\"code\":\"701\",\"message\":\"用户名已存在\",\"errors\":\"1\"})");
        } else if (user1 != null) {
            out.print(jsonp + "({\"code\":\"702\",\"message\":\"邮箱已存在\",\"errors\":\"1\"})");
        } else {
            boolean b = userService.add(username, password, email);
            if (b) {
                User user2 = userService.getUserByUsername(username);
                out.print(jsonp + "({\"code\":\"200\",\"message\":\"OK\",\"userid\":\"" + user2.getUserId() + "\",\"username\":\"" +
                        user2.getUsername() + "\",\"admin\":\"" + user2.getAdmin() + "\",\"errors\":\"0\"})");
            } else {
                out.print(jsonp + "({\"code\":\"703\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
            }
        }
    }
}
