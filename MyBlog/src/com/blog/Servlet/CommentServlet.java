package com.blog.Servlet;

import com.blog.Service.CommentService;
import com.blog.Entry.Comment;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/CommentServlet")
public class CommentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "add":
                add(req, resp);
                break;
            case "upd":
                update(req, resp);
                break;
            case "delete":
                delete(req, resp);
                break;
            case "list":
                getCommentByTime(req, resp);
                break;
            case "like":
                like(req, resp);
                break;
            case "comment":
                getCommentsByCountBlogId(req, resp);
                break;
            case "id":
                getCommentByBlogId(req, resp);
                break;
            case "fenye":
                getCommentFenYe(req,resp);
                break;
            default:
                break;
        }
    }

    protected void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CommentService commentService = new CommentService();
        String blogId = req.getParameter("blogId");
        String commentContent = req.getParameter("commentContent");
        String commentName = req.getParameter("commentName");
        if(commentName.isEmpty())
            commentName="匿名评论";
        Comment comment = new Comment();
        comment.setBlogId(Integer.parseInt(blogId));
        comment.setCommentContent(commentContent);
        comment.setCommentName(commentName);
        boolean b = commentService.add(comment);
        if (b) {
            out.print(jsonp + "({\"code\":\"200\",\"message\":\"评论已发表\",\"errors\":\"0\"})");
        } else {
            out.print(jsonp + "({\"code\":\"703\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CommentService commentService = new CommentService();
        String commentId = req.getParameter("commentId");
        boolean b = commentService.delete(commentId);
        if (b) {
            out.println(jsonp + "({\"code\":\"200\",\"message\":\"删除成功\",\"errors\":\"0\"})");
        } else {
            out.println(jsonp + "({\"code\":\"700\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CommentService commentService = new CommentService();
        String commentId = req.getParameter("id");
        String commentContent = req.getParameter("commentContent");
        Comment comment = new Comment();
        comment.setCommentId(Integer.parseInt(commentId));
        comment.setCommentContent(commentContent);
        boolean b = commentService.update(comment);
        if (b) {
            out.print(jsonp + "({\"code\":\"200\",\"message\":\"修改成功\",\"errors\":\"0\"})");
        } else {
            out.print(jsonp + "({\"code\":\"703\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void like(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CommentService commentService = new CommentService();
        String commentId = req.getParameter("commentid");
        boolean b = commentService.like(commentId);
        if (b) {
            out.print(jsonp + "({\"code\":\"200\",\"message\":\"点赞成功\",\"errors\":\"0\"})");
        } else {
            out.print(jsonp + "({\"code\":\"703\",\"message\":\"服务器不在状态，请稍后再试...\",\"errors\":\"1\"})");
        }
    }

    protected void getCommentByTime(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        int num = Integer.parseInt(req.getParameter("num"));
        CommentService commentService = new CommentService();
        List<Comment> list = commentService.getCommentsByTime(num);
        StringBuffer buffer = new StringBuffer();
        int i = 1;
        for (Comment comment : list) {
            buffer.append("{");
            buffer.append("\"commentId\":\"" + comment.getCommentId() + "\",");
            buffer.append("\"blogId\":\"" + comment.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + comment.getBlogTitle() + "\",");
            buffer.append("\"commentContent\":\"" + comment.getCommentContent() + "\",");
            buffer.append("\"commentName\":\"" + comment.getCommentName() + "\",");
            buffer.append("\"commentTime\":\"" + comment.getCommentTime() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([" + buffer + "])");
    }

    protected void getCommentsByCountBlogId(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        int num = Integer.parseInt(req.getParameter("num"));
        CommentService commentService = new CommentService();
        List<Comment> list = commentService.getCommentsByCountBlogId(num);
        StringBuffer buffer = new StringBuffer();
        int i = 1;
        for (Comment comment : list) {
            buffer.append("{");
            buffer.append("\"blogId\":\"" + comment.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + comment.getBlogTitle() + "\",");
            buffer.append("\"num\":\"" + comment.getCountBlogId() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([" + buffer + "])");
    }

    protected void getCommentByBlogId(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CommentService commentService = new CommentService();
        int blogId = Integer.parseInt(req.getParameter("blogid"));
        List<Comment> list = commentService.getCommentsByBlogId(blogId);
        StringBuffer buffer = new StringBuffer();
        int i = 1;
//        if(!list.isEmpty())
        for (Comment comment : list) {
            buffer.append("{");
            buffer.append("\"commentId\":\"" + comment.getCommentId() + "\",");
            buffer.append("\"blogId\":\"" + comment.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + comment.getBlogTitle() + "\",");
            buffer.append("\"commentContent\":\"" + comment.getCommentContent() + "\",");
            buffer.append("\"commentName\":\"" + comment.getCommentName() + "\",");
            buffer.append("\"commentTime\":\"" + comment.getCommentTime() + "\",");
            buffer.append("\"commentLike\":\"" + comment.getCommentLike() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([" + buffer + "])");

    }

    protected void getCommentByCommentName(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    protected void getCommentFenYe(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        CommentService commentService = new CommentService();
        int pageNo = Integer.parseInt(req.getParameter("pageNo"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));
        List<Comment> list = commentService.getCommentsFenYe(pageNo,pageSize);
        StringBuffer buffer = new StringBuffer();
        int count=commentService.getRowCount();
        int i = 1;
        for (Comment comment : list) {
            buffer.append("{");
            buffer.append("\"commentId\":\"" + comment.getCommentId() + "\",");
            buffer.append("\"blogId\":\"" + comment.getBlogId() + "\",");
            buffer.append("\"blogTitle\":\"" + comment.getBlogTitle() + "\",");
            buffer.append("\"commentContent\":\"" + comment.getCommentContent() + "\",");
            buffer.append("\"commentName\":\"" + comment.getCommentName() + "\",");
            buffer.append("\"commentTime\":\"" + comment.getCommentTime() + "\",");
            buffer.append("\"commentLike\":\"" + comment.getCommentLike() + "\"");
            buffer.append("}");
            if (i++ < list.size()) {
                buffer.append(",");
            }
        }
        out.print(jsonp + "([{\"page\":\"" + pageNo + "\",\"total\":\"" + count + "\"}," + buffer + "])");
    }

    protected void getRowCount(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
