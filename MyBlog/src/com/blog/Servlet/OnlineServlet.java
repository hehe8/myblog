package com.blog.Servlet;

import com.blog.Entry.Blog;
import com.blog.Service.BlogService;
import com.blog.Tools.CountUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/OnlineServlet")
public class OnlineServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        ServletContext context = req.getServletContext();
        int olNum ;
        if (context.getAttribute("OlCount") != null) {
            olNum = (int) context.getAttribute("OlCount");
        } else {
            olNum = 1;
        }int allNum;
        if (context.getAttribute("count") != null) {
            allNum = (int) context.getAttribute("count");
        } else {
            allNum = 1;
        }
        out.println(jsonp + "({\"code\":\"200\",\"allNum\":\"" + allNum + "\",\"olNum\":\"" + olNum + "\",\"errors\":\"0\"})");
    }
}
