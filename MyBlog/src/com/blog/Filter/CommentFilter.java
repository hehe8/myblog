package com.blog.Filter;

import com.blog.Entry.Minganci;
import com.blog.Service.CommentService;
import com.blog.Service.MinganciService;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class ContentFilter
 */
@WebFilter("/CommentServlet")
public class CommentFilter implements Filter {
    List<String> mgc = null;

    /**
     * Default constructor.
     */
    public CommentFilter() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
        // TODO Auto-generated method stub
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // TODO Auto-generated method stub
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        resp.setContentType("application/json;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String jsonp = req.getParameter("jsonCallback");
        if (!(req.getParameter("action").equals("add") || req.getParameter("action").equals("upd"))) {
            chain.doFilter(request, response);
            return;
        }
        MinganciService minganciService = new MinganciService();
        String commentContent = req.getParameter("commentContent");
        if (mgc == null) {
            mgc = new ArrayList<>();
            mgc = minganciService.getAll();
        }
        for (String str : mgc) {
            if (commentContent.contains(str)) {
                out.println(jsonp+"({\"code\":\"707\",\"message\":\"含有非法字符,评论失败...\",\"errors\":\"1\"})");
                return;
            }
        }
        // pass the request along the filter chain
        chain.doFilter(request, response);
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }

}
