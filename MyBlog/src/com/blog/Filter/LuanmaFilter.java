package com.blog.Filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

@WebFilter
public class LuanmaFilter implements Filter {
    @Override
    public void destroy() {
        System.out.println("过滤器销毁.......");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        System.out.println("进行过滤器开发.......");
        request.setCharacterEncoding("utf-8");// 请求
        response.setCharacterEncoding("utf-8");// 响应
        response.setContentType("text/html;charset=utf-8");// 响应
        chain.doFilter(new MyRequest((HttpServletRequest) request), response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("过滤器初始化.......");
    }

    class MyRequest extends HttpServletRequestWrapper {
        HttpServletRequest request;

        public MyRequest(HttpServletRequest request) {
            super(request);
            this.request = request;
        }

        @Override
        public String getParameter(String name) {
            String value = request.getParameter(name);
            if (value == null) {
                return null;
            }
            if (!request.getMethod().equals("GET")) {
                return value;
            }
            try {
                value = new String(value.getBytes("iso8859-1"), "utf-8");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return value;
        }

        @Override
        public String[] getParameterValues(String name) {
            String[] oldValues = request.getParameterValues(name);
            String[] newValues = null;
            if (oldValues != null) {
                newValues = new String[oldValues.length];
                if (!request.getMethod().equals("GET")) {
                    return oldValues;
                }
                if (newValues != null) {
                    try {
                        for (int i = 0; i < oldValues.length; i++) {
                            String value = oldValues[i];
                            value = new String(value.getBytes("iso8859-1"),
                                    "utf-8");
                            newValues[i] = value;
                        }
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            return newValues;
        }

        @Override
        public Map<String, String[]> getParameterMap() {
            Map<String, String[]> oldMap = request.getParameterMap();
            Map<String, String[]> newMap = new HashMap<String, String[]>();
            Set<Entry<String, String[]>> entrySet = oldMap.entrySet();
            for (Entry<String, String[]> entry : entrySet) {
                String key = entry.getKey();
                String[] values = entry.getValue();
                String[] newValues = new String[values.length];
                if (values == null) {
                    newMap.put(key, new String[]{});// 初始化
                    continue;
                }
                try {
                    for (int i = 0; i < values.length; i++) {
                        String value = values[i];
                        value = new String(value.getBytes("iso8859-1"), "utf-8");
                        newValues[i] = value;
                    }
                    newMap.put(key, newValues);
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return newMap;
        }
    }
}