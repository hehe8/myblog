package com.blog.Listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class OnlineListener implements HttpSessionListener {


    public int OlCount = 0;//记录session的数量

    public void sessionCreated(HttpSessionEvent arg0) {//监听session的创建
        System.out.println("OlCount++");
        OlCount++;
        arg0.getSession().getServletContext().setAttribute("OlCount", OlCount);

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent arg0) {//监听session的撤销
        System.out.println("OlCount--");
        OlCount--;
        arg0.getSession().getServletContext().setAttribute("OlCount", OlCount);
    }

}
