package com.blog.Listener;

import javax.servlet.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

//@WebListener("")
public class MyServletContextListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
//        System.out.println("MyBlog application is Destroyed.");
        // 获取 ServletContext 对象
        ServletContext context = servletRequestEvent.getServletContext();
        // 从 Web 应用范围获得计数器
        int count = 0;
        if (context.getAttribute("count") != null) {
            count = (int) context.getAttribute("count");
        }
        if (count != 0) {
            try {
                // 把计数器的数值写到 count.txt 文件中
                String filepath = context.getRealPath("/count");
                filepath = filepath + "/count.txt";
                PrintWriter pw = new PrintWriter(filepath);
                pw.println(count);
                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
//        System.out.println("MyBlog application is Initialized.");
        // 获取 ServletContext 对象
        ServletContext context = sre.getServletContext();
        try {
            // 从文件中读取计数器的数值
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResourceAsStream("/count/count.txt")));
            int count = Integer.parseInt(reader.readLine());
            reader.close();  // 把计数器对象保存到 Web 应用范围
            context.setAttribute("count", ++count);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}