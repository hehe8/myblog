package com.blog.Dao;

import com.blog.Entry.Blog;

import java.util.List;

public interface BlogDao  {
    public boolean add(Blog blog);
    public boolean delete(String blogId);
    public boolean update(Blog blog);
    public boolean addReadCount(String categoryId);
    public List<Blog> getAllBlog();
    public Blog getBlogById(String blogId);
    public  List<Blog> getBlogByCategoryId(String blogId,int pageNo, int pageSize);
    public List<Blog> getBlogByBlogTitle(String blogTitle,int pageNo, int pageSize);
    public List<Blog> getBlogByBlogDate(String blogDateTime,int pageNo, int pageSize);
    public List<Blog> getBlogByBlogDate(int pageNo,int pageSize);
    public List<Blog> getBlogByBlogReadCount(int pageSize);
    public List<Blog> getBlogFenYe(int pageNo, int pageSize);
    public int getRowCount();
    public int getRowCountByBlogTitle(String blogTitle);
    public int getRowCountByCategoryId(String categoryId);

}
