package com.blog.Dao;

import com.blog.Entry.Blog;
import com.blog.Entry.User;

import java.util.List;

public interface UserDao {
    public boolean add(User user);

    public boolean delete(String userId);

    public boolean update(User user);

    public boolean updPassword(User user);

    public List<User> getUserFenYe(int pageNo, int pageSize);

    public List<User> getAllUser();

    public User getUserById(String userId);

    public User getUserByUsername(String username);

    public User getUserByEmail(String email);

    public int getRowCount();

}
