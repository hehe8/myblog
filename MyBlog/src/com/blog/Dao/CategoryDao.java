package com.blog.Dao;


import com.blog.Entry.Blog;
import com.blog.Entry.Category;

import java.util.List;

public interface CategoryDao {

    public boolean add(Category category);

    public boolean delete(String categoryId);

    public boolean update(Category category);

    public List<Category> getAllCategory();

    public List<Category> getCategoryFenYe(int pageNo, int pageSize);

    public Category getCategoryById(int categoryId);

    public int getRowCount();
}
