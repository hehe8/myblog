package com.blog.Dao;

import com.blog.Entry.Comment;

import java.util.List;

public interface CommentDao {
    public boolean add(Comment comment);

    public boolean delete(String commentId);

    public boolean like(String commentId);

    public boolean update(Comment comment);

    public List<Comment> getCommentsByBlogId(int blogId);

    public List<Comment> getCommentsByTime(int num);//num 数量；

    public List<Comment> getCommentsByCountBlogId(int num);//num 数量；

    public List<Comment> getCommentsFenYe(int pageNo, int pageSize);//num 数量；

    public int getRowCount();
}
