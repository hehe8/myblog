package com.blog.Tools;


import java.sql.*;

public class DataBase {
    final String USERNAME = "root";
    final String PASSWORD = "181451";
    String URL = "jdbc:mysql://localhost:3306/myblog";
    String DRIVER="com.mysql.jdbc.Driver";
    Connection conn = null;
    PreparedStatement ps = null;
    public ResultSet rs = null;
    public DataBase() {
        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void closeAll() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public boolean executeUpdate(String sql, Object... args) {
        boolean tag = false;
        try {
            ps = conn.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);
            }
            int result = ps.executeUpdate();
            if (result > 0) {
                tag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tag;
    }

    public ResultSet executeQuery(String sql, Object... args) {
        try {
            ps = conn.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                ps.setObject(i + 1, args[i]);
            }
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }


}
