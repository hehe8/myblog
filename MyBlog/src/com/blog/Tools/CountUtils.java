package com.blog.Tools;

public class CountUtils {
    private static int loginCount = 0;

    public static void add() {
        loginCount++;
    }

    public static void subtract() {
        loginCount--;
    }

    public static int getLoginCount() {
        return loginCount;

    }
}