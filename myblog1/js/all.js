//美化
$(function() {
	clearclear();
	beautyHeader();
	beautySidebar();
	var url = document.location;
	if (String(url).indexOf("blog.html") != -1) {
		beautyComments();
	}
})

//登录控制
window.onload = function() {
	get_login();
	var url = document.location;
	setTimeout(function() {
		$(".blocks").fadeOut();
		$("#home").show();
	}, 2000)
}



function get_login() {
	$(".blocks").fadeOut();
	$("#home").show();
	var currentuser = sessionStorage.getItem("CURRENTUSERS");
	if (currentuser != null && currentuser != '') {
		var jsonObject = JSON.parse(currentuser);
		if (jsonObject.code == 200) {
			$("#on_login").html(jsonObject.username);
			$("#not_login").hide();
			$("#logout").show();
			if (jsonObject.admin == 1) {
				$('#control').show();
				$('#writeblog').show();
			} else {
				$('#control').hide();
				$('#writeblog').hide();
			}
		}
	}
}

function getScrollTop() {
	var scrollTop = 0,
		bodyScrollTop = 0,
		documentScrollTop = 0;
	if (document.body) {
		bodyScrollTop = document.body.scrollTop;
	}
	if (document.documentElement) {
		documentScrollTop = document.documentElement.scrollTop;
	}
	scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
	return scrollTop;
}

function getScrollHeight() {
	var scrollHeight = 0,
		bodyScrollHeight = 0,
		documentScrollHeight = 0;
	if (document.body) {
		bodyScrollHeight = document.body.scrollHeight;
	}
	if (document.documentElement) {
		documentScrollHeight = document.documentElement.scrollHeight;
	}
	scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
	return scrollHeight;
}

function getWindowHeight() {
	var windowHeight = 0;
	if (document.compatMode == "CSS1Compat") {
		windowHeight = document.documentElement.clientHeight;
	} else {
		windowHeight = document.body.clientHeight;
	}
	return windowHeight;
}

var get_to_top_button = $('#get-to-top');

function get_to_top() {
	setTimeout(function() {
		get_to_top_button.className = 'get-to-top btn-hide';
	}, 300);
	(function smoothscroll() {
		var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
		if (currentScroll > 0) {
			window.requestAnimationFrame(smoothscroll);
			window.scrollTo(0, currentScroll - (currentScroll / 5));
		}
	})();
}

window.onscroll = function() {
	var ToTopHeight = getWindowHeight() * 1.5;
	if (getScrollTop() + getWindowHeight() > ToTopHeight) {
		get_to_top_button.removeClass("btn-hide")
		get_to_top_button.addClass("btn-show");
	} else {
		get_to_top_button.removeClass("btn-show")
		get_to_top_button.addClass("btn-hide");
	}
}



var head = $("#header");
var headerFullScreen = true;



function headerMax() { //页首全屏
	var url = document.location;
	if (String(url).indexOf("index") != -1) {
		head.css({
			'height': '100vh',
			'max-width': '100vw',
			'border-radius': '0'
		});
		$('body').css({
			"overflow-y": "hidden"
		});
		headerFullScreen = true;
	}
}

function headerMin() { //取消页首全屏
	head.css({
		'height': '480px',
		'max-width': '1100px',
		'border-radius': '0 0 16px 16px'
	});
	setTimeout(function() {
		$('body').css({
			"overflow-y": "auto"
		})
	}, 250);
	headerFullScreen = false;
}

function initHeaderStyle() { //初始化页首样式
	head.css('background-image', 'url(//open.saintic.com/api/bingPic/)');
	if (getScrollTop() == 0) {
		headerMax();
	} else {
		headerMin();
	}
}

function beautyHeader() { //美化页首
	initHeaderStyle();
	setTimeout(function() {
		head.css('transition', 'all cubic-bezier(0.4, 0, 0.2, 1) 0.4s')
	}, 300);
	head.on('mousewheel', function(event) {
		if (getScrollTop() == 0) {
			if (event.originalEvent.wheelDelta >= 0 && headerFullScreen == false) { //Scroll up
				headerMax();
			} else if (event.originalEvent.wheelDelta < 0 && headerFullScreen == true) { //Scroll down
				headerMin();
			}
		}
	});
	head.on('DOMMouseScroll', function(event) {
		if (getScrollTop() == 0) {
			if (event.detail < 0 && headerFullScreen == false) { //Scroll up
				headerMax();
			} else if (event.detail >= 0 && headerFullScreen == true) { //Scroll down
				headerMin();
			}
		}
	});
}


function breakOnedayPost(articleList) { //同一天文章列表分离
	articleList.each(function() {
		var post = $(this);
		var dayTitle = post.children('.dayTitle');
		var postTitle = post.children('.postTitle');
		var postCon = post.children('.postCon');
		var postDesc = post.children('.postDesc');
		if (postTitle.length > 1) {
			postTitle.each(function(i) {
				var now = $('<div class="day"></div>');
				now.append(dayTitle.clone(), postTitle.eq(i), postCon.eq(i), postDesc.eq(i));
				post.before(now);
			});
			post.remove();
		}
	});
}

function addPostImg(articleList) { //添加文章卡片特色图片
	articleList.each(function() {
		var post = $(this);
		var post_left = $('<div class="post-left"></div>');
		var post_right = $('<div class="post-right"></div>');
		var desc_img = post.find('.desc_img');
		// alert(desc_img);
		if (desc_img.length) {
			post.addClass('have-img');
			var url = desc_img.attr('src');
			desc_img.remove();
			var div_have_img = $('<div class="img-layer"></div>');
			div_have_img.css('background-image', 'url(' + url + ')');
			post_left.append(post.find('.dayTitle'), post.find('.c_b_p_desc_readmore'), div_have_img);
			post_right.append(post.find('.postTitle'), post.find('.postCon'), post.find('.postDesc'));
			post.empty();
			post.append(post_left);
			post.append(post_right);
		} else {
			post.addClass('no-img');
			post.find('.clear').remove();
		}
	});
}

function beautyPostCard() { //美化文章卡片
	breakOnedayPost($('.day'));
	addPostImg($('.day'));
}


var colors = ['green', 'blue', 'yellow', 'red'],
	index = 0;
var getColor = function() {
	(index >= colors.length) && (index = 0);
	return colors[index++];
};


function dianzan(id) {
	_alert("点赞成功");
	$("#" + id).attr("onclick", "_alert('已经点过赞啦...')");
}

function share(id) {
	_alert("分享成功");
	$("#" + id).attr("onclick", "_alert('已经分享过啦...')");
}

function wechat() {
	_alert('<img src="img/mm_facetoface_collect_qrcode_1591257390969.png" width="200px" >');
}

function _addFavorite() {
	var url = window.location;
	var title = document.title;
	var ua = navigator.userAgent.toLowerCase();
	// alert(1);
	if (ua.indexOf("360se") > -1) {
		_alert('由于360浏览器功能限制，请按 Ctrl+D 手动收藏！');
	} else if (ua.indexOf("msie 8") > -1) {
		window.external.AddToFavoritesBar(url, title); //IE8
		_alert('收藏成功')
	} else if (document.all) { //IE类浏览器
		try {
			window.external.addFavorite(url, title);
			_alert('收藏成功')
		} catch (e) {
			_alert('您的浏览器不支持,请按 Ctrl+D 手动收藏!');
		}
	} else if (window.sidebar) { //firfox等浏览器；
		window.sidebar.addPanel(title, url, "");
		_alert('收藏成功')
	} else {
		_alert('您的浏览器不支持,请按 Ctrl+D 手动收藏!');
	}
}

function beautyCommentIcon(n) { //美化评论区按钮图标
	var self = $(n);
	var comment_actions = self.find('.comment_actions');
	self.find('.feedbackManage').empty().append(comment_actions);
	self.find('.comment_actions').children('a').each(function() {
		var now = $(this);
		if (now.text().match('回复')) {
			now.addClass('comment-relpy');
			now.html('<i class="iconfont icon-reply"></i>');

		} else if (now.text().match('引用')) {
			now.addClass('comment-quote');
			now.html('<i class="iconfont icon-format-quote"></i>');

		} else if (now.text().match('删除')) {
			now.addClass('comment-delete');
			now.html('<i class="iconfont icon-delete"></i>');

		} else if (now.text().match('修改')) {
			now.addClass('comment-edit');
			now.html('<i class="iconfont icon-edit"></i>');
		}
	});
	var digg = self.find('.comment_digg');
	var digg_num = parseInt(digg.text().substr(digg.text().indexOf('(') + 1));
	digg.html('<i class="iconfont icon-thumb-up"></i>' + '<span>' + digg_num + '</span>');
	var burry = self.find('.comment_burry');
	var burry_num = parseInt(burry.text().substr(burry.text().indexOf('(') + 1));
	burry.html('<i class="iconfont icon-thumb-down"></i>' + '<span>' + burry_num + '</span>');
}

function beautyComments() { //美化评论区
	$(document).ajaxSuccess(function(event, xhr, option) {
		if (String(document.location).match("blog.html")) {
			setTimeout(function() {
				//无评论提示
				if ($('#blog-comments-placeholder').html().length == 0)
					$('#blog-comments-placeholder').append('<p>还没有评论，快来抢沙发</p>');
				//评论框占位文本
				$("#tbCommentBody").attr("placeholder", "来啊，快活啊!");

				//添加图标
				$(".feedbackItem").each(function() {
					beautyCommentIcon(this);
				})
				//换页按钮美化
				$('#comment_pager_bottom .pager a').each(function() {
					var self = $(this);
					if (self.text().match('Prev')) {
						self.html('<i class="iconfont icon-arrow-round-back"></i>');
					} else if (self.text().match('Next')) {
						self.html('<i class="iconfont icon-arrow-round-forward"></i>');
					}
				})
			}, 200);
		};
	});
}


function beautyCatTags() { //美化标签和分类
	$('#sidebar_toptags li').each(function(index, ele) {
		var self = $(ele);
		var cat = self.children('a');
		cat.text('#' + cat.text().trim());
		self.empty().append(cat);
	});
	$('#sidebar_postcategory li a').each(function(index, ele) {
		var self = $(ele);
		if (self.text().indexOf('(') > -1) {
			self.text(self.text().substr(0, self.text().indexOf('(')));
		}
		self.text('#' + self.text().trim());
		self.addClass('color-' + (index % 8 + 1));
	});
	$('#sidebar_toptags li a').each(function(index, ele) {
		var self = $(ele);
		self.addClass('color-' + (index % 8 + 1));
	});
}

function beautySearchBox() { //美化搜索框
	var tigger = $('.btn_my_zzk').attr('onclick');
	$('.btn_my_zzk').replaceWith('<button class="btn_my_zzk" onclick="' + tigger +
		'"><i class="iconfont icon-search"></i></button>');
}

function addSidebarIcon() { //添加侧边栏标题图标
	//去除标题前后空白符
	var shortcut = $('#sidebar_shortcut').find('.catListTitle');
	var recentposts = $('#sidebar_recentposts').find('.catListTitle');
	var toptags = $('#sidebar_toptags').find('.catListTitle');
	var postcategory = $('#sidebar_postcategory').find('.catListTitle');
	var links = $('#sidebar_links1034473').find('.catListTitle');
	var recentcomments = $('#sidebar_recentcomments').find('.catListTitle');
	shortcut.text(shortcut.text().trim());
	recentposts.text(recentposts.text().trim());
	toptags.text(toptags.text().trim());
	postcategory.text(postcategory.text().trim());
	links.text(links.text().trim());
	recentcomments.text(recentcomments.text().trim());

}

function addSidebarNewsIcon() { //添加侧边栏公告标题图标
	//去除标题前后空白符
	var news = $('#sidebar_news').find('.catListTitle');
	news.text(news.text().trim());
	//添加图标
	$('#sidebar_news').find('.catListTitle').prepend('<i class="iconfont icon-event-note"></i>');
}

function addSidebarTopListIcon() { //添加侧边栏排行榜标题图标
	//去除标题前后空白符
	var topviewedposts = $('#sidebar_topviewedposts').find('.catListTitle');
	topviewedposts.text(topviewedposts.text().trim());
	var topdiggedposts = $('#sidebar_topdiggedposts').find('.catListTitle');
	topdiggedposts.text(topdiggedposts.text().trim());
	//添加图标
	$('#sidebar_topviewedposts').find('.catListTitle').prepend('<i class="iconfont icon-vertical-align-top"></i>');
	$('#sidebar_topdiggedposts').find('.catListTitle').prepend('<i class="iconfont icon-thumb-up"></i>');
}

function beautySidebar() { //美化侧边栏
	$(document).ajaxSuccess(function(event, xhr, option) {
		if (option.url.match("calendar.aspx")) {
			beautyCalendar();
		}
		if (option.url.match("sidecolumn.aspx")) {
			beautySearchBox();
			beautyCatTags();
			addSidebarIcon();
		}
		if (option.url.match("TopLists.aspx")) {
			addSidebarTopListIcon();
		}
		if (option.url.match("news.aspx")) {
			addSidebarNewsIcon();
		}
	})
}

function clearclear() {
	$('.clear').remove();
}


$(window).on("load", function() {
	$('.preloader-wapper').addClass('loaded');
	if ($('.preloader-wapper').hasClass('loaded')) {
		$('.preloader-main').delay(1200).queue(function() {
			$(this).remove();
		});
	}
});

function initCookie() {
	var username = getCookie('username');
	var password = getCookie('password');
	$("#loginUsername").val(username);
	$("#loginPassword").val(password);
}


//写cookies
function setCookie(name, value) {
	var Days = 1;
	var exp = new Date();
	exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
	document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

//读取cookies 
function getCookie(name) {
	var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
	if (arr = document.cookie.match(reg)) return unescape(arr[2]);
	else return null;
}


function logout() {
	sessionStorage.removeItem("CURRENTUSERS");
	delCookie("password");
	delCookie("username");
	document.location.href = 'index.html';
};
//删除cookies
function delCookie(name) {
	var exp = new Date();
	exp.setTime(exp.getTime() - 1);
	document.cookie = name + "=;expires=" + exp.toGMTString();
}
