var pageSize = 9; //每页显示的数目。


//显示添加的图片
function show(file) {
	var reader = new FileReader(); // 实例化一个FileReader对象，用于读取文件
	var img = document.getElementById('img'); // 获取要显示图片的标签
	//读取File对象的数据
	reader.onload = function(evt) {
		img.src = evt.target.result;
	}
	reader.readAsDataURL(file.files[0]);
}

//搜索框Enter
function search_enter(event) {
	if (event.keyCode == 13) {
		search_go();
	}

}

function search_go() {
	var title = $('#q').val();
	if (title != null && title != '')
		document.location.href = "search.html?title=" + title;
	else {
		_alert("请输入些内容再搜索...");
	}
}



//根据url里的属性名获取值
function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if (pair[0] == variable) {
			return pair[1];
		}
	}
	return (false);
}


//日期拆分
function date_sort(date) {
	var time = {};
	var f = date.split(' ', 2); //过滤空格
	if (f[0].search("/") != -1) { //判断是否包含-
		var d = (f[0] ? f[0] : '').split('/', 3); //过滤-
	} else {
		var d = (f[0] ? f[0] : '').split('-', 3); //过滤-
	}
	time.year = parseInt(d[0]); //转换成整数形式的原因是 过滤掉 月份和时分秒的首位补零的情况
	time.month = parseInt(d[1]);
	time.day = parseInt(d[2]);
	var t = (f[1] ? f[1] : '').split(':', 3); //过滤:
	time.hour = parseInt(t[0]);
	time.minute = parseInt(t[1]);
	time.second = parseInt(t[2]);
	return time;
}

function getdateTtime(date) {
	var time = {};
	var f = date.split(' ', 2); //过滤空格
	if (f[0].search("/") != -1) { //判断是否包含-
		var d = (f[0] ? f[0] : '').split('/', 3); //过滤-
	} else {
		var d = (f[0] ? f[0] : '').split('-', 3); //过滤-
	}
	var t = (f[1] ? f[1] : '').split(':', 3); //过滤:
	time.year = d[0]; //转换成整数形式的原因是 过滤掉 月份和时分秒的首位补零的情况
	time.month = d[1];
	time.day = d[2];
	time.hour = t[0];
	time.minute = t[1];
	time.second = t[2];
	return time.year + '-' + time.month + '-' + time.day + 'T' + time.hour + ':' + time.minute + ':' + time.second;
}

function getdatetime(date) {
	var time = {};
	var f = date.split(' ', 2); //过滤空格
	if (f[0].search("/") != -1) { //判断是否包含-
		var d = (f[0] ? f[0] : '').split('/', 3); //过滤-
	} else {
		var d = (f[0] ? f[0] : '').split('-', 3); //过滤-
	}
	time.year = parseInt(d[0]); //转换成整数形式的原因是 过滤掉 月份和时分秒的首位补零的情况
	time.month = parseInt(d[1]);
	time.day = parseInt(d[2]);
	var t = (f[1] ? f[1] : '').split(':', 3); //过滤:
	time.hour = parseInt(t[0]);
	time.minute = parseInt(t[1]);
	time.second = parseInt(t[2]);
	return time.year + '/' + time.month + '/' + time.day + ' ' + time.hour + ':' + time.minute + ':' + time.second;
}


var colors = ['green', 'blue', 'yellow', 'red'],
	index = 0;
var getColor = function() {
	(index >= colors.length) && (index = 0);
	return colors[index++];
};

function _alert(_content) {
	new jBox('Notice', {
		animation: 'flip',
		color: getColor(),
		content: _content
	});
}

function admin() {
	var currentuser = sessionStorage.getItem("CURRENTUSERS");
	if (currentuser != null && currentuser != '') {
		var jsonObject = JSON.parse(currentuser);
		if (jsonObject.code == 200) {
			var admin = jsonObject.admin;
			if (jsonObject.admin == 1) {
				var url = document.location;
				if (String(url).indexOf("control.html") != -1) {
					controlloadData(1, 10);
					$("#control").removeAttr("href").click(function() {
						_alert("Already on the control page.");
					});
				} else if (String(url).indexOf("taglist.html") != -1) {
					backtagslist(1, 10);
				} else if (String(url).indexOf("commentlist.html") != -1) {
					backcommentlist(1, 10);
				} else if (String(url).indexOf("userlist.html") != -1) {
					userlistloadData(1, 20);
				}
			} else {
				alert("非管理员,无法进入控制台...");
				document.location.href = 'index.html';
			}
		} else {
			alert("未登录,无法进入控制台...");
			document.location.href = 'index.html';
		}
	} else {
		alert("未登录,无法进入控制台...");
		document.location.href = 'index.html';
	}

}


function loadaddblogtaglist() {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CategoryServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "list"
		},
		success: function(data) {
			var html = ""
			// alert(JSON.stringify(data));
			for (var i = 0; i < data.length; i++) {
				html += '<option value="' + data[i].categoryId + '">' + data[i].categoryName + '</option>';
			}
			$("#tagselect").html(html);
		}
	});
}


function backblogloadData(id) {
	// alert(id);
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "id",
			"blogid": id
		},
		success: function(data) {
			$("#title").val(data.blogTitle);
			$("#tagselect").val(data.categoryId);
			$("#summary").val(data.blogSummary);
			$("#img").attr("src", data.blogImgUrl);
			data1 = data.blogContent.replaceAll('\\"', '"').replaceAll('><', '>\n<');
			setTimeout(function() {
				editor.setData(data1);
			}, 500);
			$("#time").val(getdateTtime(data.blogDateTime));
		},
		error: function(data) {
			_alert("服务器不在状态...");
		}
	})
}

function loadtagbyid(id) {
	// alert(id);
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CategoryServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "id",
			"id": id
		},
		success: function(data) {
			$("#tagname").val(data.categoryName);
			$("#tagcontent").val(data.categoryContent);
		},
		error: function(data) {
			_alert("服务器不在状态...");
		}
	})
}

function loaduserbyid(id) {
	// alert(id);
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/UserServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "id",
			"id": id
		},
		success: function(data) {
			$("#username").val(data.username);
			$("#useremail").val(data.email);
			if (data.admin == '1') {
				$("input[type='radio']").get(0).checked = true;
			}
		},
		error: function(data) {
			_alert("服务器不在状态...");
		}
	})
}

String.prototype.replaceAll = function(s1, s2) {
	return this.replace(new RegExp(s1, "gm"), s2);
}

function addblog() {
	var title = $("#title").val();
	var time = $("#time").val();
	var summary = $("#summary").val();
	var tagselect = $("#tagselect").val();
	var imgUrl = "img/" + $("#imgUrl").val().substring(12);
	// editor.setData('这里是需要传递给CKEditor编辑器实例的值');
	var content = editor.getData().replace(/[\n\r]/g, '').replaceAll('"', '\\"').replaceAll('>	<', '><');
	// alert(title);alert(time);alert(summary);alert(imgUrl);alert(tagselect);
	// alert(content.length);
	var json = {
		"action": "add",
		"title": title,
		"time": time,
		"summary": summary,
		"imgUrl": imgUrl,
		"content": content,
		"categoryId": tagselect
	};
	console.log(JSON.stringify(json));
	//C:\fakepath\1559545447236.jpg
	$.ajax({
		type: "POST",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		// async: true,
		dataType: "json",
		// jsonp: "jsonCallback",
		data: json,
		success: function(data) {
			_alert(data.message);
			if (data.code == 200) {
				_alert("三秒后返回列表页")
				setTimeout(function() {
					document.location.href = 'control.html'
				}, 3000);
			}
		},
		error: function() {
			_alert("服务器不在状态...");
		}
	})
}

function updtag(id) {
	var name = $("#tagname").val();
	var content = $("#tagcontent").val();
	if (name == '' || name == null) {
		_alert("标签名称不能为空...");
		return false;
	} else if (content == '' || content == null) {
		_alert("标签介绍不能为空...");
		return false;
	}
	var json = {
		"action": "upd",
		"categoryid": id,
		"name": name,
		"content": content
	};
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CategoryServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: json,
		success: function(data) {
			_alert(data.message);
			if (data.code == 200) {
				_alert("三秒后返回列表页")
				setTimeout(function() {
					document.location.href = 'taglist.html'
				}, 3000);
			}
		},
		error: function() {
			_alert("服务器不在状态...");
		}
	})
}



function upduser(id) {
	var name = $("#username").val();
	var email = $("#useremail").val();
	var admin = $('input:radio:checked').val();
	if (name == '' || name == null) {
		_alert("用户名称不能为空...");
		return false;
	} else if (email == '' || email == null) {
		_alert("邮箱不能为空...");
		return false;
	}
	var json = {
		"action": "upd",
		"userid": id,
		"username": name,
		"email": email,
		"admin": admin
	};
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/UserServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: json,
		success: function(data) {
			_alert(data.message);
			if (data.code == 200) {
				_alert("三秒后返回列表页")
				setTimeout(function() {
					document.location.href = 'userlist.html'
				}, 3000);
			}
		},
		error: function() {
			_alert("服务器不在状态...");
		}
	})
}

function updpassword(id) {
	var oldpassword = $("#oldpassword").val();
	var password = $("#password").val();
	var password2 = $("#password2").val();
	if (oldpassword == '' || oldpassword == null) {
		_alert("旧密码不能为空...");
		return false;
	} else if (password == '' || password == null) {
		_alert("新密码不能为空...");
		return false;
	} else if (oldpassword == password) {
		_alert("新密码与旧密码相同...")
		return false;
	} else if (password != password2) {
		_alert("两次新密码相同..")
		return false;
	} else if (password.length < 6) {
		_alert("密码长度小于六位...");
		return false;
	}
	var json = {
		"action": "password",
		"userid": id,
		"oldpassword": oldpassword,
		"password": password
	};
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/UserServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: json,
		success: function(data) {
			_alert(data.message);
			if (data.code == 200) {
				$("#pass").hide(500);
				$("#oldpassword").val("");
				$("#password").val("");
				$("#password2").val("");
				$("#updpass").attr("onclick", "");
			}
		},
		error: function() {
			_alert("服务器不在状态...");
		}
	})
}

function addtag() {
	var name = $("#tagname").val();
	var content = $("#tagcontent").val();
	if (name == '' || name == null) {
		_alert("标签名称不能为空...");
		return false;
	} else if (content == '' || content == null) {
		_alert("标签介绍不能为空...");
		return false;
	}
	var json = {
		"action": "add",
		"name": name,
		"content": content
	};
	$.ajax({
		type: "POST",
		url: "http://localhost:8090/MyBlog/CategoryServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: json,
		success: function(data) {
			_alert(data.message);
			if (data.code == 200) {
				_alert("三秒后返回列表页")
				setTimeout(function() {
					document.location.href = 'taglist.html'
				}, 3000);
			}
		},
		error: function() {
			_alert("服务器不在状态...");
		}
	})
}

function updblog(id) {
	var title = $("#title").val();
	var time = $("#time").val();
	var summary = $("#summary").val();
	var tagselect = $("#tagselect").val();
	var imgUrl = '';
	if ($("#imgUrl").val() != '' && $("#imgUrl").val() != null) {
		imgUrl = "img/" + $("#imgUrl").val().substring(12);
	} else {
		imgUrl = $("#img").attr("src");
	}
	// editor.setData('这里是需要传递给CKEditor编辑器实例的值');
	var content = editor.getData().replace(/[\n\r]/g, '').replaceAll('"', '\\"');
	$.ajax({
		type: "POST",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		// async: true,
		dataType: "json",
		// jsonp: "jsonCallback",
		data: {
			"action": "upd",
			"blogid": id,
			"title": title,
			"time": time,
			"summary": summary,
			"imgUrl": imgUrl,
			"content": content,
			"categoryId": tagselect
		},
		success: function(data) {
			_alert(data.message);
			if (data.code == 200) {
				_alert("三秒后返回列表页")
				setTimeout(function() {
					document.location.href = 'control.html'
				}, 3000);
			}
		},
		error: function() {
			_alert("服务器不在状态...");
		}
	})
}

function delblog(blogId) {
	var r = confirm("确定要删除这篇博客吗?");
	if (r == true) {
		$.ajax({
			type: "get",
			url: "http://localhost:8090/MyBlog/BlogServlet",
			async: true,
			dataType: "jsonp",
			jsonp: "jsonCallback",
			data: {
				"action": "del",
				"blogId": blogId
			},
			success: function(data) {
				_alert(data.message);
				$("#blogtr_" + blogId).remove();
			},
			error: function() {
				_alert("服务器不在状态...");
			}
		})
	}
}

function deluser(userId) {
	var r = confirm("确定要删除这个用户吗?");
	if (r == true) {
		$.ajax({
			type: "get",
			url: "http://localhost:8090/MyBlog/UserServlet",
			async: true,
			dataType: "jsonp",
			jsonp: "jsonCallback",
			data: {
				"action": "del",
				"userId": userId
			},
			success: function(data) {
				_alert(data.message);
				$("#usertr_" + userId).remove();
			},
			error: function() {
				_alert("服务器不在状态...");
			}
		})
	}
}

function deltag(id) {
	var r = confirm("确定要删除这个标签吗?");
	if (r == true) {
		$.ajax({
			type: "get",
			url: "http://localhost:8090/MyBlog/CategoryServlet",
			async: true,
			dataType: "jsonp",
			jsonp: "jsonCallback",
			data: {
				"action": "del",
				"categoryId": id
			},
			success: function(data) {
				_alert(data.message);
				$("#tagtr_" + id).remove();
			},
			error: function() {
				_alert("服务器不在状态...");
			}
		})
	}
}

function delcomment(id) {
	var r = confirm("确定要删除这个评论吗?");
	if (r == true) {
		$.ajax({
			type: "get",
			url: "http://localhost:8090/MyBlog/CommentServlet",
			async: true,
			dataType: "jsonp",
			jsonp: "jsonCallback",
			data: {
				"action": "delete",
				"commentId": id
			},
			success: function(data) {
				_alert(data.message);
				$("#commenttr_" + id).remove();
			},
			error: function() {
				_alert("服务器不在状态...");
			}
		})
	}
}

//index页获取列表数据
function indexloadData(pageNo, pageSize) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "list",
			"pageNo": pageNo,
			"pageSize": pageSize
		},
		success: function(data) {
			var html = "";
			// alert(JSON.stringify(data));
			for (var i = 1; i < data.length; i++) {
				var datestr = data[i].blogDateTime;
				var datetime = date_sort(datestr);
				var date = datetime.year + "-" + datetime.month + "-" + datetime.day;
				var time = datetime.hour + ":" + datetime.minute + ":" + datetime.second;
				html += '<div class="day">';
				html += '<div class="dayTitle">';
				html += '<a href="archive.html?date=' + datetime.year + "-" + datetime.month + '">' + datetime.year + '/' +
					datetime.month + '/' + datetime.day +
					'</a>';
				html += '</div>';
				html += '<div class="postTitle">';
				html += '<a class="postTitle2" href="blog.html?id=' + data[i].blogId + '">';
				html += data[i].blogTitle;
				html += '</a></div><div class="postCon"><div class="c_b_p_desc">';
				html += '摘要：' + data[i].blogSummary;
				html += '<img src="' + data[i].blogImgUrl + '" class="desc_img"> ';
				html += '<a href="blog.html?id=' + data[i].blogId + '" class="c_b_p_desc_readmore">阅读全文</a>';
				html += '</div></div><div class="clear"></div>';
				html += '<div class="postDesc">posted @ ' + date + ' ' + time + ' HDAWN ';
				html += '<span class="post-view-count">阅读(' + data[i].blogReadCount + ')</span><text> </text>';
				html += '<span class="post-comment-count">评论(' + data[i].commentNum + ')</span><text> </text>';
				html += '</div><div class="clear"></div></div>';
			}
			$("#bloglist").html(html);
			pageseting(pageNo, data[0].total, pageSize);
			beautyPostCard();
		}
	});
}

//Control页获取列表数据
function controlloadData(pageNo, pageSize) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "list",
			"pageNo": pageNo,
			"pageSize": pageSize
		},
		success: function(data) {
			var html = "";
			// alert(JSON.stringify(data));
			for (var i = 1; i < data.length; i++) {
				var datestr = data[i].blogDateTime;
				var datetime = getdatetime(datestr);
				html += '<tr class="gradeX" id="blogtr_' + data[i].blogId + '"><td><img src="';
				html += data[i].blogImgUrl;
				html += '"class="tpl-table-line-img" alt=""></td><td class="am-text-middle">';
				html += '<a href="blog.html?id=' + data[i].blogId + '">' + data[i].blogTitle + '</a>';
				html += '</td><td class="am-text-middle">';
				html += data[i].blogReadCount;
				html += '</td><td class="am-text-middle">';
				html += data[i].commentNum;
				html += '</td><td class="am-text-middle">';
				html += datetime;
				html += '</td><td class="am-text-middle"><div class="tpl-table-black-operation">';
				html += '<a href="updblogs.html?blogId=' + data[i].blogId;
				html += '" class="Tooltip-3" title="编辑">	<i class="am-icon-pencil"></i></a>';
				html += '<a onclick="delblog(' + data[i].blogId;
				html += ')" class="Tooltip-3 tpl-table-black-operation-del" title="删除">';
				html += '<i class="am-icon-trash"></i></a></div></td></tr>';
			}
			$("#tbodybloglist").html(html);
			pageseting(pageNo, data[0].total, pageSize);
		}
	});
}

function userlistloadData(pageNo, pageSize) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/UserServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "list",
			"pageNo": pageNo,
			"pageSize": pageSize
		},
		success: function(data) {
			var html = "";
			// alert(JSON.stringify(data));
			for (var i = 1; i < data.length; i++) {
				html += '<tr class="gradeX" id="usertr_' + data[i].userId + '"><td class="am-text-middle">';
				html += data[i].username + '</td><td class="am-text-middle">';
				html += data[i].email + '</td><td class="am-text-middle">' + data[i].admin +
					'</td><td class="am-text-middle"><div class="tpl-table-black-operation">';
				html += '<a href="updusers.html?id=' + data[i].userId + '" class="Tooltip-3" title="编辑">';
				html += '<i class="am-icon-pencil"></i></a><a onclick="deluser(' + data[i].userId;
				html += ')" class="Tooltip-3 tpl-table-black-operation-del" title="删除"><i class="am-icon-trash"></i>';
				html += '</a></div></td></tr>';
			}
			$("#tbodybloglist").html(html);
			pageseting(pageNo, data[0].total, pageSize);
		}
	});
}

//blog load date
function blogloadData(id) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "id",
			"blogid": id
		},
		success: function(data) {
			// console.log((JSON.stringify(data)));
			$("#cb_post_title_url").html(data.blogTitle);
			$("title").html(data.blogTitle);
			$("#cnblogs_post_body").html(data.blogContent + '<img src="' + data.blogImgUrl + '" width="100%"/>');
			$("#post-date").html(getdatetime(data.blogDateTime));
			$("#post_view_count").html(data.blogReadCount);
			$("#post_comment_count").html(data.commentNum);
			$("#btn_comment_submit").attr("onclick", "addComment(" + id + ")");
			$("#BlogPostCategory").append('<a href="category.html?id=' + data.categoryId + '&name=' + data.categoryName +
				'" target="_blank">' + data.categoryName + '(' + data.blogNum + ')' + '</a>')
		},
		error: function(data) {
			_alert("服务器不在状态...");
			beautyPostCard();
		}
	})
	loadComment(id);
}

function loadComment(id) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CommentServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "id",
			"blogid": id
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			html = "";
			for (var i = 0; i < data.length; i++) {
				var span_id = "commentlike_" + String(data[i].commentId);
				html +=
					'<div class="feedbackItem"><div class="feedbackListSubtitle"><span class="comment_date"><i class="iconfont icon-time"></i>';
				html += getdatetime(data[i].commentTime);
				html += '</span><a href="javascript:void(0);" target="_blank">';
				html += data[i].commentName;
				html +=
					'</a></div><div class="feedbackCon"><img src="img/1102665-20181104182953302-371660734.jpg" style="float:left;" class="author_avatar"><div id="comment_body_4536275" data-format-type="Markdown" class="blog_comment_body cnblogs-markdown"><p>';
				html += data[i].commentContent.replaceAll('\\"', '"').replaceAll('><', '>\n<');
				html +=
					'</p></div><div class="comment_vote"><span class="comment_error" style="color: red"></span><a href="javascript:void(0);" class="comment_digg" onclick="like(';
				html += data[i].commentId;
				html += ')" id="' + span_id + '" ><i class="iconfont icon-thumb-up"></i><span>';
				html += data[i].commentLike;
				html += '</span></a></div></div></div>';
			}
			// alert(html);
			$(".feedbackItem").remove();
			if (html == '') {
				$("#not_comment").show();
			} else {
				$("#not_comment").hide();
				$("#blog-comments-placeholder").append(html);
			}
		},
		error: function(data) {
			_alert("服务器不在状态...");
		}
	})
}

//评论点赞
function like(id) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CommentServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "like",
			"commentid": id
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			_alert(data.message);
			if (data.code == 200) {
				$("#commentlike_" + id).html(parseInt($("#commentlike_" + id).text()) + 1).attr("onclick",
					"_alert('已经点过赞啦..')");

			}
		},
		error: function(data) {
			_alert("服务器不在状态...");
		}
	})

}



function addCommentListener() {
	$("#zishu").html($("#commenttext").html().replace(/[\n\r]/g, '').replaceAll('"', '\\"').length);
	if (event.ctrlKey && window.event.keyCode == 13)
		$('#btn_comment_submit').click();
}
//评论
function addComment(blogid) {
	var commenttext = $("#commenttext").html().replace(/[\n\r]/g, '').replaceAll('"', '\\"');
	// alert(commenttext.length);
	if (commenttext == '' || commenttext == null) {
		_alert("请输入评论内容..")
		return;
	}
	var commentName = '';
	var currentuser = sessionStorage.getItem("CURRENTUSERS");
	if (currentuser != null && currentuser != '') {
		var jsonObject = JSON.parse(currentuser);
		if (jsonObject.code == 200) {
			commentName = jsonObject.username;
		}
	}
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CommentServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "add",
			"blogId": blogid,
			"commentContent": commenttext,
			"commentName": commentName
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			_alert(data.message);
			if (data.code == 200) {
				loadComment(blogid);
				$("#markwrite").val("");
				$("#commenttext").html("");
			}
		},
		error: function(data) {
			_alert("服务器不在状态...");
		}
	})
}




//search页获取列表数据
function archiveloadData(date1, pageNo, pageSize) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "date",
			"date": date1,
			"pageNo": pageNo,
			"pageSize": pageSize
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			var html = "";
			for (var i = 1; i < data.length; i++) {
				var datestr = data[i].blogDateTime;
				var datetime = date_sort(datestr);
				var date = datetime.year + "-" + datetime.month + "-" + datetime.day;
				var time = datetime.hour + ":" + datetime.minute + ":" + datetime.second;
				html += '<div class="day">';
				html += '<div class="dayTitle">';
				html += '<a href="archive.html?date=' + datetime.year + "-" + datetime.month + '">';
				html += datetime.year + '/' + datetime.month + '/' + datetime.day + '</a></div>';
				html += '<div class="postTitle">';
				html += '<a class="postTitle2" href="blog.html?id=' + data[i].blogId + '">';
				html += data[i].blogTitle;
				html += '</a></div><div class="postCon"><div class="c_b_p_desc">';
				html += '摘要：' + data[i].blogSummary;
				html += '<img src="' + data[i].blogImgUrl + '" class="desc_img"> ';
				html += '<a href="blog.html?id=' + data[i].blogId + '" class="c_b_p_desc_readmore">阅读全文</a>';
				html += '</div></div><div class="clear"></div>';
				html += '<div class="postDesc">posted @ ' + date + ' ' + time + ' HDAWN ';
				html += '<span class="post-view-count">阅读(' + data[i].blogReadCount + ')</span><text> </text>';
				html += '<span class="post-comment-count">评论(' + data[i].commentNum + ')</span><text> </text>';
				html += '</div><div class="clear"></div></div>';
			}
			// alert(html);
			if (html != '') {
				$(".day").remove();
				$("#bloglist").append(html);
			} else {
				$(".day").remove();
				alert(date)
				$("#bloglist").append("<blockquote style='font-size:20px;' ></br>未找到“ " + date1 +
					" ”的博客文章</br></br></blockquote>");


			}
			pageseting(pageNo, data[0].total, pageSize);
			beautyPostCard();
		},
		error: function(data) {
			_alert("服务器不在状态...");
			beautyPostCard();
		}
	});
}

function backtagslist(pageNo, pageSize) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CategoryServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "fenye",
			"pageNo": pageNo,
			"pageSize": pageSize
		},
		success: function(data) {
			var html = ""
			// alert(JSON.stringify(data));
			for (var i = 1; i < data.length; i++) {
				html += '<tr class="gradeX" id="tagtr_' + data[i].categoryId +
					'"><td class="am-text-middle"><a href="category.html?id=';
				html += +data[i].categoryId + '&name=' + data[i].categoryName + '">' + data[i].categoryName + '</a></td>';
				html += '<td class="am-text-middle">' + data[i].categoryContent + '</td><td class="am-text-middle">';
				html += data[i].blogNum + '</td><td class="am-text-middle"><div class="tpl-table-black-operation">';
				html += '<a href="updtags.html?id=' + data[i].categoryId + '" class="Tooltip-3" title="编辑"><i class="';
				html += 'am-icon-pencil"></i></a><a onclick="deltag(' + data[i].categoryId + ')" class="Tooltip-3';
				html += ' tpl-table-black-operation-del" title="删除"><i class="am-icon-trash"></i></a></div></td></tr>';
			}
			$("#tbodytagslist").html(html);
			pageseting(pageNo, data[0].total, pageSize);
		}
	});
}

function backcommentlist(pageNo, pageSize) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CommentServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "fenye",
			"pageNo": pageNo,
			"pageSize": pageSize
		},
		success: function(data) {
			var html = ""
			// alert(JSON.stringify(data));
			for (var i = 1; i < data.length; i++) {
				html += '<tr class="gradeX" id="commenttr_' + data[i].commentId +
					'"><td class="am-text-middle"><a href="blog.html?id=';
				html += +data[i].blogId + '">' + data[i].commentContent + '</a></td>';
				html += '<td class="am-text-middle">' + data[i].commentName + '</td><td class="am-text-middle">' + getdatetime(
					data[i].commentTime) + '</td><td class="am-text-middle">';
				html += data[i].commentLike + '</td><td class="am-text-middle"><div class="tpl-table-black-operation">';
				html += '<a style="display:none;" href="updcomments.html?id=' + data[i].commentId +
					'" class="Tooltip-3" title="编辑"><i class="';
				html += 'am-icon-pencil"></i></a><a onclick="delcomment(' + data[i].commentId + ')" class="Tooltip-3';
				html += ' tpl-table-black-operation-del" title="删除"><i class="am-icon-trash"></i></a></div></td></tr>';
			}
			$("#tbodytagslist").html(html);
			pageseting(pageNo, data[0].total, pageSize);
		}
	});
}
//search页获取列表数据
function categoryloadData(id, name, pageNo, pageSize) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "categoryid",
			"categoryid": id,
			"pageNo": pageNo,
			"pageSize": pageSize
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			var html = "";
			for (var i = 1; i < data.length; i++) {
				var datestr = data[i].blogDateTime;
				var datetime = date_sort(datestr);
				var date = datetime.year + "-" + datetime.month + "-" + datetime.day;
				var time = datetime.hour + ":" + datetime.minute + ":" + datetime.second;
				html += '<div class="day">';
				html += '<div class="dayTitle">';
				html += '<a href="archive.html?date=' + datetime.year + "-" + datetime.month + '">';
				html += datetime.year + '/' + datetime.month + '/' + datetime.day + '</a></div>';
				html += '<div class="postTitle">';
				html += '<a class="postTitle2" href="blog.html?id=' + data[i].blogId + '">';
				html += data[i].blogTitle;
				html += '</a></div><div class="postCon"><div class="c_b_p_desc">';
				html += '摘要：' + data[i].blogSummary;
				html += '<img src="' + data[i].blogImgUrl + '" class="desc_img"> ';
				html += '<a href="blog.html?id=' + data[i].blogId + '" class="c_b_p_desc_readmore">阅读全文</a>';
				html += '</div></div><div class="clear"></div>';
				html += '<div class="postDesc">posted @ ' + date + ' ' + time + ' HDAWN ';
				html += '<span class="post-view-count">阅读(' + data[i].blogReadCount + ')</span><text> </text>';
				html += '<span class="post-comment-count">评论(' + data[i].commentNum + ')</span><text> </text>';
				html += '</div><div class="clear"></div></div>';
			}
			// alert(html);
			if (html != '') {
				$(".day").remove();
				$("#bloglist").append(html);
			} else {
				$(".day").remove();
				// $("#bloglist").append("<blockquote style='font-size:20px;' ></br>未找到关于“ " + name +
				// " ”的博客文章</br></br></blockquote>");
				$("#bloglist").append("<blockquote style='font-size:20px;' ></br>" + "这个类别还没有博客</br></br></blockquote>");

			}
			pageseting(pageNo, data[0].total, pageSize);
			beautyPostCard();
		},
		error: function(data) {
			_alert("服务器不在状态...");
			beautyPostCard();
		}
	});
}
//search页获取列表数据
function searchloadData(title, pageNo, pageSize) {
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "title",
			"title": title,
			"pageNo": pageNo,
			"pageSize": pageSize
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			var html = "";
			for (var i = 1; i < data.length; i++) {
				var datestr = data[i].blogDateTime;
				var datetime = date_sort(datestr);
				var date = datetime.year + "-" + datetime.month + "-" + datetime.day;
				var time = datetime.hour + ":" + datetime.minute + ":" + datetime.second;
				html += '<div class="day">';
				html += '<div class="dayTitle">';
				html += '<a href="archive.html?date=' + datetime.year + "-" + datetime.month + '">';
				html += datetime.year + '/' + datetime.month + '/' + datetime.day + '</a></div>';
				html += '<div class="postTitle">';
				html += '<a class="postTitle2" href="blog.html?id=' + data[i].blogId + '">';
				html += data[i].blogTitle;
				html += '</a></div><div class="postCon"><div class="c_b_p_desc">';
				html += '摘要：' + data[i].blogSummary;
				html += '<img src="' + data[i].blogImgUrl + '" class="desc_img"> ';
				html += '<a href="blog.html?id=' + data[i].blogId + '" class="c_b_p_desc_readmore">阅读全文</a>';
				html += '</div></div><div class="clear"></div>';
				html += '<div class="postDesc">posted @ ' + date + ' ' + time + ' HDAWN ';
				html += '<span class="post-view-count">阅读(' + data[i].blogReadCount + ')</span><text> </text>';
				html += '<span class="post-comment-count">评论(' + data[i].commentNum + ')</span><text> </text>';
				html += '</div><div class="clear"></div></div>';
			}
			// alert(html);
			if (html != '') {
				$(".day").remove();
				$("#bloglist").append(html);
			} else {
				$(".day").remove();
				$("#bloglist").append("<blockquote style='font-size:20px;' ></br>未找到关于“ " + title +
					" ”的博客文章</br></br></blockquote>");

			}
			pageseting(pageNo, data[0].total, pageSize);
			beautyPostCard();
		},
		error: function(data) {
			_alert("服务器不在状态...");
			beautyPostCard();
		}
	});
}

var sidebarSize = 5; //最新博客，排行榜的数目。
function loadSideBarData() {
	//在线人数
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/OnlineServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		success: function(data) {
			$("#allnum").html(data.allNum);
			$("#olnum").html(data.olNum);
		},
		error: function(data) {
			_alert("服务器不在状态...");
			beautyPostCard();
		}
	});
	//博客档//最新博客
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "list",
			"pageNo": 1,
			"pageSize": sidebarSize
		},
		success: function(data) {
			var html = "";
			for (var i = 1; i < Math.min(sidebarSize + 1, data.length); i++) {
				html += '<li><a href="blog.html?id=' + data[i].blogId + '" title="' +
					data[i].blogTitle + '">' + String(i) + '.' + data[i].blogTitle + '</a></li>';
			}
			$("#newblog").html(html);
		},
		error: function(data) {
			_alert("服务器不在状态...");
			beautyPostCard();
		}
	});
	//博客档案
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "datelist",
			"pageNo": 1,
			"pageSize": sidebarSize
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			var html = "";
			for (var i = 0; i < data.length; i++) {
				html += '<li><a href="archive.html?date=' + data[i].date + '" >';
				html += data[i].date + '(' + data[i].num + ')</a></li>';
			}
			$("#blogfile").html(html);
		}
	});
	//博客标签
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CategoryServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "list"
		},
		success: function(data) {
			var html = ""
			// alert(JSON.stringify(data));
			for (var i = 0; i < data.length; i++) {
				html += '<li><a href="category.html?id=' + data[i].categoryId +
					'&name=' + data[i].categoryName + '" rel="" target="" class="color-' + String((i %
						7 + 1)) + '">#' + data[i].categoryName + '</a></li>';
			}
			$("#categorylist").html(html);
		}
	});
	//阅读排行榜
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/BlogServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "read",
			"pageSize": sidebarSize
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			var html = ""
			for (var i = 0; i < data.length; i++) {

				html += '<li><a href="blog.html?id=' + data[i].blogId + '" title="' +
					data[i].blogTitle + '(' + data[i].blogReadCount + ')">' + String(i + 1) +
					'.' + data[i].blogTitle + '(' + data[i].blogReadCount + ')</a></li>';

			}
			$("#readlist").html(html);
		}
	});
	//评论排行榜
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CommentServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "comment",
			"num": sidebarSize
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			var html = ""
			for (var i = 0; i < data.length; i++) {

				html += '<li><a href="blog.html?id=' + data[i].blogId + '" title="' +
					data[i].blogTitle + '(' + data[i].num + ')">' + String(i + 1) +
					'.' + data[i].blogTitle + '(' + data[i].num + ')</a></li>';
			}
			$("#countlist").html(html);
		}
	});
	//最新评论
	$.ajax({
		type: "get",
		url: "http://localhost:8090/MyBlog/CommentServlet",
		async: true,
		dataType: "jsonp",
		jsonp: "jsonCallback",
		data: {
			"action": "list",
			"num": sidebarSize
		},
		success: function(data) {
			// alert(JSON.stringify(data));
			var html = ""
			for (var i = 0; i < data.length; i++) {
				html += '<li class="recent_comment_title" style="background:#FFEDF4;"><a href="blog.html?id=' + data[i].blogId +
					'" title="' + String(i + 1) + '.' + data[i].blogTitle + '">' + String(i + 1) +
					'.' + data[i].blogTitle + '</a></li>';
				html += '<li class="recent_comment_body">';
				html += '<p>' + data[i].commentContent + '</p></li>';
				html += '<li class="recent_comment_title"><span style="font-size: 11px;">' + getdatetime(data[i].commentTime) +
					'</span> --' + data[i].commentName + '<hr color="red" height="50px" width="100%"></li>';

			}
			$("#commentlist").html(html);
		}
	});
}

function pageseting(pageNo, totalData, pageSize) {
	$.fn.zPager.defaults = {
		totalData: totalData, //数据总条数
		pageData: pageSize, //每页数据条数
		pageCount: Math.ceil(totalData / pageSize), //总页数
		current: pageNo, //当前页码数
		pageStep: 8, //当前可见最多页码个数
		minPage: 5, //最小页码数，页码小于此数值则不显示上下分页按钮
		active: 'current', //当前页码样式
		prevBtn: 'pg-prev', //上一页按钮
		nextBtn: 'pg-next', //下一页按钮
		btnBool: true, //是否显示上一页下一页
		firstBtn: 'pg-first', //第一页按钮
		lastBtn: 'pg-last', //最后一页按钮
		btnShow: true, //是否显示第一页和最后一页按钮
		disabled: true, //按钮失效样式
		ajaxSetData: false, //是否使用ajax获取数据 此属性为真时需要url和htmlBox不为空
		url: '', //ajax路由
		htmlBox: '' //ajax数据写入容器
	}
	$("#pager").zPager({
		totalData: totalData,
		htmlBox: $('#bloglist'),
		btnShow: true,
		ajaxSetData: false
	});
	$("#pager a").click(function() {
		get_to_top();
		var pageid = $(this).attr("page-id");
		// alert(pageid)
		if (pageid != 0 && pageid != (Math.ceil(totalData / pageSize) + 1)) {
			var url = document.location;
			if ((String(url).indexOf("index") != -1)) {
				indexloadData(pageid, pageSize);
			} else if (String(url).indexOf("search") != -1) {
				var title = decodeURI(getQueryVariable("title"));
				searchloadData(title, pageid, pageSize);
			} else if (String(url).indexOf("control") != -1) {
				controlloadData(pageid, pageSize);
			} else if (String(url).indexOf("taglist") != -1) {
				backtagslist(pageid, pageSize);
			} else if (String(url).indexOf("category") != -1) {
				var id = decodeURI(getQueryVariable("id"));
				categoryloadData(id, pageid, pageSize);
			} else if (String(url).indexOf("archive") != -1) {
				var date = decodeURI(getQueryVariable("date"));
				archiveloadData(date, pageid, pageSize);
			} else if (String(url).indexOf("commentlist") != -1) {
				backcommentlist(pageid, pageSize);
			} else if (String(url).indexOf("userlist") != -1) {
				userlistloadData(pageid, pageSize);
			}
		}
	});
}
